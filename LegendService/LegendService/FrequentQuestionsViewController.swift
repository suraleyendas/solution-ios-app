//
//  FrequentQuestionsViewController.swift
//  LegendService
//
//  Created by informatica on 15/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class FrequentQuestionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    var selectedItemIndex: Int?
    
    var cells: SwiftyAccordionCells!
    
    override func viewDidLoad() {
        cells = SwiftyAccordionCells()
        self.tableView.estimatedRowHeight = 45
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.allowsMultipleSelection = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadTableData()
    }
    
    func loadTableData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "FaqAPI/GetFAQS/1", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if let reportArray = dataResp["ArrayFAQ"] as? [[String: Any]]{
                                for reportObj in reportArray{
                                    self.cells.append(SwiftyAccordionCells.HeaderItem(id: (reportObj["Id"] as? Int)!, question: (reportObj["Question"] as? String)!, answer: (reportObj["Answer"] as? String)!))
                                    self.cells.append(SwiftyAccordionCells.Item(id: (reportObj["Id"] as? Int)!, question: (reportObj["Question"] as? String)!, answer: (reportObj["Answer"] as? String)!))
                                }
                                self.tableView.reloadData()
                                self.stopAnimating()
                                self.tableView.flashScrollIndicators()
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadTableData);
                }
            }
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cells.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.cells.items[(indexPath as NSIndexPath).row]
            
        if item as? SwiftyAccordionCells.HeaderItem != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "faqCell") as! FAQTableViewCell
            cell.accessoryType = .none
            cell.question.text = item.question
            return cell
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "simpleCell") {
                cell.textLabel?.text = item.answer
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.sizeToFit()
                return cell
            }else{
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.cells.items[(indexPath as NSIndexPath).row]
        
        if item is SwiftyAccordionCells.HeaderItem {
            return 60
        } else if (item.isHidden) {
            return 0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.cells.items[(indexPath as NSIndexPath).row]
        
        if item is SwiftyAccordionCells.HeaderItem {
            
            let cell = tableView.cellForRow(at: indexPath) as! FAQTableViewCell
            cell.contentView.backgroundColor = UIColor.init(red: 37/255.0, green: 100/255.0, blue: 138/255.0, alpha: 1)
            cell.icon.image = UIImage(named: "show_white_icon.png")
            cell.question.textColor = UIColor.white
            
            if self.selectedHeaderIndex == nil {
                self.selectedHeaderIndex = (indexPath as NSIndexPath).row
            } else {
                self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                self.selectedHeaderIndex = (indexPath as NSIndexPath).row
            }
            
            if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                self.cells.collapse(previouslySelectedHeaderIndex)
            }
            
            if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                self.cells.expand(self.selectedHeaderIndex!)
            } else {
                self.selectedHeaderIndex = nil
                self.previouslySelectedHeaderIndex = nil
            }
            
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let item = self.cells.items[(indexPath as NSIndexPath).row]
        
        if item is SwiftyAccordionCells.HeaderItem {
            
            let cell = tableView.cellForRow(at: indexPath) as! FAQTableViewCell
            cell.contentView.backgroundColor = UIColor.white
            cell.icon.image = UIImage(named: "hide_icon.png")
            cell.question.textColor = UIColor.init(red: 37/255.0, green: 100/255.0, blue: 138/255.0, alpha: 1)
            
            if self.selectedHeaderIndex == nil {
                self.selectedHeaderIndex = (indexPath as NSIndexPath).row
            } else {
                self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                self.selectedHeaderIndex = (indexPath as NSIndexPath).row
            }
            
            if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                self.cells.collapse(previouslySelectedHeaderIndex)
            }
            
            if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                self.cells.expand(self.selectedHeaderIndex!)
            } else {
                self.selectedHeaderIndex = nil
                self.previouslySelectedHeaderIndex = nil
            }
            
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            
        }
    }

    @IBAction func backFreqQuest(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
