//
//  TermsViewController.swift
//  LegendService
//
//  Created by informatica on 15/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        userDefaults.set(1, forKey: "root_view")
        
        let parameters = [
            "ParameterId": 1,
            "ProgramId": 1
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ParameterAPI/TermsAndConditions", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    if let jsonData = returnData["data"] as? [String: Any] {
                        DispatchQueue.main.sync(execute: {
                            self.textView.text =  String(describing: jsonData["Value"])
                            self.stopAnimating()
                        });
                    }
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }
    }

    @IBAction func backTerms(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
