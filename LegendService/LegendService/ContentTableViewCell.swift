//
//  ContentTableViewCell.swift
//  LegendService
//
//  Created by informatica on 14/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ContentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pictureProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var viewImageConst: NSLayoutConstraint!
    @IBOutlet weak var imageContent: UIImageView!
    @IBOutlet weak var imageVideoConst: NSLayoutConstraint!
    @IBOutlet weak var videoCont: UIWebView!
    @IBOutlet weak var videoTextConst: NSLayoutConstraint!
    @IBOutlet weak var textContent: UILabel!
    @IBOutlet weak var viewUserCommentaryConst: NSLayoutConstraint!
    @IBOutlet weak var userCommentaryCont: UILabel!
    @IBOutlet weak var userCommentaryFirstCommentConst: NSLayoutConstraint!
    @IBOutlet weak var firstCommentaryCont: UIView!
    @IBOutlet weak var firstCommentaryImg: UIImageView!
    @IBOutlet weak var firstCommentaryName: UILabel!
    @IBOutlet weak var firstCommentaryText: UILabel!
    @IBOutlet weak var firstCommentarySecondCommentConst: NSLayoutConstraint!
    @IBOutlet weak var secondCommentaryCont: UIView!
    @IBOutlet weak var secondCommentaryImg: UIImageView!
    @IBOutlet weak var secondCommentaryName: UILabel!
    @IBOutlet weak var secondCommentaryText: UILabel!
    @IBOutlet weak var secondCommentaryImgConst: NSLayoutConstraint!
    @IBOutlet weak var firstCommentaryImgConst: NSLayoutConstraint!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var commentaryCounter: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentaryBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pictureProfile.layer.borderWidth = 2
        pictureProfile.layer.masksToBounds = false
        pictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfile.layer.cornerRadius = min(pictureProfile.frame.height, pictureProfile.frame.width)/2.0
        pictureProfile.clipsToBounds = true
        
        firstCommentaryImg.layer.borderWidth = 2
        firstCommentaryImg.layer.masksToBounds = false
        firstCommentaryImg.layer.borderColor = UIColor.lightGray.cgColor
        firstCommentaryImg.layer.cornerRadius = min(firstCommentaryImg.frame.height, firstCommentaryImg.frame.width)/2.0
        firstCommentaryImg.clipsToBounds = true
        
        secondCommentaryImg.layer.borderWidth = 2
        secondCommentaryImg.layer.masksToBounds = false
        secondCommentaryImg.layer.borderColor = UIColor.lightGray.cgColor
        secondCommentaryImg.layer.cornerRadius = min(secondCommentaryImg.frame.height, secondCommentaryImg.frame.width)/2.0
        secondCommentaryImg.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
