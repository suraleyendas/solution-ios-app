//
//  SwiftyAccordionCells.swift
//  SwiftyAccordionCells
//
//  Created by Fischer, Justin on 9/24/15.
//  Copyright © 2015 Justin M Fischer. All rights reserved.
//

import Foundation

class SwiftyAccordionCells {
    var items = [Item]()
    
    class Item {
        var isHidden: Bool
        var isChecked: Bool
        var id = 0
        var question = ""
        var answer = ""
        
        init(_ hidden: Bool = true, checked: Bool = false, id: Int, question: String, answer: String) {
            self.isHidden = hidden
            self.isChecked = checked
            self.id = id
            self.question = question
            self.answer = answer
        }
    }
    
    class HeaderItem: Item {
        init (id: Int, question: String, answer: String) {
            super.init(false, checked: false, id: id, question: question, answer: answer)
        }
    }
    
    func append(_ item: Item) {
        self.items.append(item)
    }
    
    func removeAll() {
        self.items.removeAll()
    }
    
    func expand(_ headerIndex: Int) {
        self.toogleVisible(headerIndex, isHidden: false)
    }
    
    func collapse(_ headerIndex: Int) {
        self.toogleVisible(headerIndex, isHidden: true)
    }
    
    private func toogleVisible(_ headerIndex: Int, isHidden: Bool) {
        var headerIndex = headerIndex
        headerIndex += 1
        
        while headerIndex < self.items.count && !(self.items[headerIndex] is HeaderItem) {
            self.items[headerIndex].isHidden = isHidden
            
            headerIndex += 1
        }
    }
}
