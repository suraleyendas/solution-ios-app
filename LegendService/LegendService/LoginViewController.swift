//
//  LoginViewController.swift
//  LegendService
//
//  Created by informatica on 14/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var userBoxView: UIView!
    @IBOutlet weak var passwordBoxView: UIView!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var employeeBtn: UIButton!
    @IBOutlet weak var adviserBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var choiseEmployeeTipe = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(2, forKey: "root_view")
        
        userBoxView.layer.cornerRadius = 5
        passwordBoxView.layer.cornerRadius = 5
        
        //Up view
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Hide keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func choiseEmployee(_ sender: Any) {
        choiseEmployeeTipe = 1
        updateRadioButton("on", "off", "off")
    }
    
    @IBAction func choiseAdviser(_ sender: Any) {
        choiseEmployeeTipe = 2
        updateRadioButton("off", "on", "off")
    }

    @IBAction func choiseOther(_ sender: Any) {
        choiseEmployeeTipe = 3
        updateRadioButton("off", "off", "on")
    }
    
    func updateRadioButton(_ r1 : String, _ r2 : String, _ r3 : String){
        employeeBtn.setImage(UIImage(named: "radio_btn_\(r1).png") as UIImage?, for: .normal)
        adviserBtn.setImage(UIImage(named: "radio_btn_\(r2).png") as UIImage?, for: .normal)
        otherBtn.setImage(UIImage(named: "radio_btn_\(r3).png") as UIImage?, for: .normal)
    }

    @IBAction func signIn(_ sender: Any) {
        logIn()
    }
    
    func logIn(){
        var isFormFilled = true
        let color = UIColor.red
        if(username.text == ""){
            isFormFilled = false
            username.placeholder = "Ingrese el usuario"
            username.attributedPlaceholder = NSAttributedString(string: username.placeholder!, attributes: [NSForegroundColorAttributeName : color])
        }
        
        if(password.text == ""){
            isFormFilled = false
            password.placeholder = "Ingrese la contraseña"
            password.attributedPlaceholder = NSAttributedString(string: password.placeholder!, attributes: [NSForegroundColorAttributeName : color])
        }
        
        if(isFormFilled){
            
            let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
            let parameters = [
                "userName": username.text!,
                "password": password.text!,
                "ipAddress":"10.10.10.10",
                "programId":"1",
                "userTypeId": choiseEmployeeTipe
                ] as [String : Any]
            
            ServerHandler().sendRequest(parameters, "UsersAPI/Login", "POST") {
                returnData in
                print("Data \(returnData)")
                if let status = returnData["status"] as? Bool {
                    if(status){
                        if let responseData = returnData["data"] as? [String : Any] {
                            let userDefaults = UserDefaults.standard
                            let UserId = responseData["UserId"] as? Int;
                            userDefaults.set(UserId, forKey: "user_id")
                            DispatchQueue.main.sync(execute: {
                                let nextViewController = self.storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                                self.present(nextViewController, animated:true, completion:nil)
                                self.stopAnimating()
                            });
                        }
                    }else{
                        DispatchQueue.main.sync(execute: {
                            self.stopAnimating()
                        })
                        Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.logIn);
                    }
                }
            }
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        username.resignFirstResponder()
        password.resignFirstResponder()
        return true
    }
}
