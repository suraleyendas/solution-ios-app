//
//  Comentary.swift
//  LegendService
//
//  Created by informatica on 22/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class Commentary: NSObject {
    
    var commentParentid:Int = 0
    var userId = 0
    var userImage:String = ""
    var userName:String = ""
    var text = ""
    
    init(commentParentid:Int, userId:Int, userImage:String, userName:String, text:String){
        
        self.commentParentid = commentParentid
        self.userId = userId
        self.userImage = userImage
        self.userName = userName
        self.text = text
        
    }
    
    override init(){
        
    }
}
