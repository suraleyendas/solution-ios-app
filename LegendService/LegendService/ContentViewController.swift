//
//  ContentViewController.swift
//  LegendService
//
//  Created by informatica on 14/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pagetitle: UILabel!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var type = ""
    var user = User()
    var publicationArray = [Publication]()
    
    var imgHeigt = CGFloat()
    var videoHeight = CGFloat()
    var textHeiht = CGFloat()
    var firstCommentaryContHeight = CGFloat()
    var firstCommentaryImgHeight = CGFloat()
    var firstCommentaryNameHeight = CGFloat()
    var firstCommentaryTextHeight = CGFloat()
    var secondCommentaryContHeight = CGFloat()
    var secondCommentaryImgHeight = CGFloat()
    var secondCommentaryNameHeight = CGFloat()
    var secondCommentaryTextHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 600
        
        if(type == "content_saved"){
            pagetitle.text = "Contenidos guardados"
        }else{
            pagetitle.text = "Contenidos que me gustan"
        }
        
        loadUserData()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(forName: .UIContentSizeCategoryDidChange, object: .none, queue: OperationQueue.main) { [weak self] _ in
            self?.tableView.reloadData()
        }
        
        loadTableData()
    }

    func loadUserData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUser/\(userId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            self.user.pictureProfile = (response["Picture"] as? String)!
                            self.user.name = (response["Name"] as? String)!
                            self.user.email = (response["Email"] as? String)!
                        }else{
                            Utils().createDissmisAlert("Presentamos inconvenientes con el servicio al recuperar usuario. Por favor intetelo mas tarde.", self)
                        }
                        self.stopAnimating()
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadUserData);
                }
            }
        }
    }
    
    func loadTableData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId": userId,
            "TopComment": 2,
            "PageIndex": 0,
            "PageSize": 100
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/GetWallByUser", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let respData = returnData["data"] as? [String: Any] {
                            if let userPublicationArray = respData["arrayContent"] as? [[String: Any]] {
                                self.publicationArray = [Publication]()
                                
                                for userPublication in userPublicationArray{
                                    
                                    var commentaries = [Commentary]()
                                    let publicationCommentaries = userPublication["ListComment"] as? [[String: Any]]
                                    for publicationCommentary in publicationCommentaries!{
                                        commentaries.append(
                                            Commentary.init(
                                                commentParentid: (publicationCommentary["ContentParentId"] as? Int)!,
                                                userId: (publicationCommentary["UserId"] as? Int)!,
                                                userImage: (publicationCommentary["Picture"] as? String)!,
                                                userName: (publicationCommentary["FullName"] as? String)!,
                                                text: (publicationCommentary["StrComment"] as? String)!
                                            )
                                        )
                                    }
                                    
                                    self.publicationArray.append(
                                        Publication.init(
                                            id: (userPublication["ContentId"] as? Int)!,
                                            userId: (userPublication["UserId"] as? Int)!,
                                            filePath: (userPublication["FilePath"] as? String)!,
                                            userCommentary: (userPublication["strContent"] as? String)!,
                                            isActive: (userPublication["IsActive"] as? Bool)!,
                                            contentTypeId: (userPublication["ContentTypeId"] as? Int)!,
                                            contentType: (userPublication["ContentType"] as? String)!,
                                            UserContentTypeId: (userPublication["UserContentTypeId"] as? Int)!,
                                            UserContentType: (userPublication["UserContentType"] as? String)!,
                                            isLike: (userPublication["IsLike"] as? Bool)!,
                                            countLike: (userPublication["CountLike"] as? Int)!,
                                            countComment: (userPublication["CountComment"] as? Int)!,
                                            picture: (userPublication["Picture"] as? String)!,
                                            name: (userPublication["Name"] as? String)!,
                                            commetaries: commentaries
                                        )
                                    )
                                }
                                self.tableView.reloadData()
                                self.stopAnimating()
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadTableData);
                }
            }
        }
    }
    
    //Table delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return publicationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contentsCell", for: indexPath) as! ContentTableViewCell
        let row = indexPath.row
        let publication = publicationArray[indexPath.row]
        
        if(publication.picture != ""){
            cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
        }else{
            cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
        }
        cell.userName.text = publication.name
        
        print("\(row) : \(publication.contentTypeId)")
        switch publication.contentTypeId {
        //Text
        case 1:
            calculateTextHeight(item: cell.textContent, textStr: publication.userCommentary, clear: true, validateHeightFrame: true)
            cell.textContent.text = publication.userCommentary
            cell.imageVideoConst.constant = 0
            hideItem(item: cell.videoCont)
            hideItem(item: cell.imageContent)
            self.view.layoutIfNeeded();
            break
        //Video
        case 2:
            cleanHeightConstraints(item: cell.videoCont, itemHeight: &videoHeight)
            cell.imageVideoConst.constant = 0
            hideItem(item: cell.imageContent)
            hideItem(item: cell.textContent)
            let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"\(publication.filePath)\" ></iframe>"
            cell.videoCont.loadHTMLString(htmlStr, baseURL: nil)
            cell.videoCont.scrollView.isScrollEnabled = false
            self.view.layoutIfNeeded();
            break
        //Image
        case 3:
            cleanHeightConstraints(item: cell.imageContent, itemHeight: &imgHeigt)
            cell.imageVideoConst.constant = 0
            hideItem(item: cell.videoCont)
            hideItem(item: cell.textContent)
            cell.imageContent.imageFromServerURL(urlString: publication.filePath)
            cell.imageContent.clipsToBounds = true
            self.view.layoutIfNeeded();
            break
        //Event
        case 4:
            cell.imageVideoConst.constant = 0
            hideItem(item: cell.videoCont)
            hideItem(item: cell.imageContent)
            print(row)
            self.view.layoutIfNeeded();
            break
        default:
            break
        }
        
        //Update counters
        cell.likeCounter.text = "\(publication.countLike)"
        cell.commentaryCounter.text = "\(publication.countComment)"
        
        //Like
        cell.likeBtn.tag = publication.id
        if(publication.isLike){
            cell.likeBtn.accessibilityHint = "like"
            cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
        }else{
            cell.likeBtn.accessibilityHint = "notLike"
            cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
        }
        cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
        
        //Comment
        cell.commentaryBtn.tag = publication.id
        cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
        if(publication.commetaries.count > 0){
            cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
        }else{
            cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
        }
        cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
        
        //Text gesture
        cell.textContent.numberOfLines = 3
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
        cell.textContent.tag = publication.id
        cell.textContent.addGestureRecognizer(tapGesture)
        
        //Share
        cell.shareBtn.tag = publication.contentTypeId
        cell.shareBtn.accessibilityHint = publication.userCommentary
        cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
        
        //show options
        cell.optionsBtn.tag = publication.id
        cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
        
        //User commentary
        var strToFit = publication.userCommentary
        if(publication.contentTypeId == 1){
            strToFit = ""
        }
        calculateTextHeight(item: cell.userCommentaryCont, textStr: strToFit, clear: true, validateHeightFrame: false)
        cell.userCommentaryCont.text = strToFit
        
        if(publication.countComment == 0){
            cell.userCommentaryFirstCommentConst.constant = 0
            hideCommentary(items: [cell.firstCommentaryCont, cell.firstCommentaryName, cell.firstCommentaryText, cell.firstCommentaryImg])
            cell.firstCommentarySecondCommentConst.constant = 0
            hideCommentary(items: [cell.secondCommentaryCont, cell.secondCommentaryName, cell.secondCommentaryText, cell.secondCommentaryImg])
            self.view.layoutIfNeeded();
        }else if(publication.countComment == 1){
            showFirstCommentary(items: [cell.firstCommentaryCont, cell.firstCommentaryImg, cell.firstCommentaryName, cell.firstCommentaryText])
            cell.firstCommentarySecondCommentConst.constant = 0
            hideCommentary(items: [cell.secondCommentaryCont, cell.secondCommentaryName, cell.secondCommentaryText, cell.secondCommentaryImg])
            self.view.layoutIfNeeded();
            
            let firstCommentary = publication.commetaries[0]
            cell.firstCommentaryName.text = firstCommentary.userName
            cell.firstCommentaryText.text = firstCommentary.text
            
            if(firstCommentary.userImage != ""){
                cell.firstCommentaryImg.image = Utils().decodeImageToBase64(firstCommentary.userImage)
            }else{
                cell.firstCommentaryImg.image = UIImage(named:"small_pic_prof.png")!
            }
        }else{
            showFirstCommentary(items: [cell.firstCommentaryCont, cell.firstCommentaryImg, cell.firstCommentaryName, cell.firstCommentaryText])
            self.view.layoutIfNeeded();
            let firstCommentary = publication.commetaries[0]
            cell.firstCommentaryName.text = firstCommentary.userName
            cell.firstCommentaryText.text = firstCommentary.text
            if(firstCommentary.userImage != ""){
                cell.firstCommentaryImg.image = Utils().decodeImageToBase64(firstCommentary.userImage)
            }else{
                cell.firstCommentaryImg.image = UIImage(named:"small_pic_prof.png")!
            }
            
            showSecondCommentary(items: [cell.secondCommentaryCont, cell.secondCommentaryImg, cell.secondCommentaryName, cell.secondCommentaryText])
            self.view.layoutIfNeeded();
            let secondCommentary = publication.commetaries[1]
            cell.secondCommentaryName.text = secondCommentary.userName
            cell.secondCommentaryText.text = secondCommentary.text
            if(secondCommentary.userImage != ""){
                cell.secondCommentaryImg.image = Utils().decodeImageToBase64(secondCommentary.userImage)
            }else{
                cell.secondCommentaryImg.image = UIImage(named:"small_pic_prof.png")!
            }
        }
        
        return cell
        
    }
    
    func hideItem(item: UIView){
        for constraint in item.constraints {
            if (constraint.firstAttribute == .height) {
                constraint.isActive = false;
            }
        }
        let heightConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([heightConstraint])
    }
    
    func hideCommentary(items: [UIView]){
        for item in items {
            hideItem(item: item)
        }
    }
    
    func showFirstCommentary(items: [UIView]){
        cleanHeightConstraints(item: items[0], itemHeight: &firstCommentaryContHeight)
        cleanHeightConstraints(item: items[1], itemHeight: &firstCommentaryImgHeight)
        cleanHeightConstraints(item: items[2], itemHeight: &firstCommentaryNameHeight)
        cleanHeightConstraints(item: items[3], itemHeight: &firstCommentaryTextHeight)
    }
    
    func showSecondCommentary(items: [UIView]){
        cleanHeightConstraints(item: items[0], itemHeight: &secondCommentaryContHeight)
        cleanHeightConstraints(item: items[1], itemHeight: &secondCommentaryImgHeight)
        cleanHeightConstraints(item: items[2], itemHeight: &secondCommentaryNameHeight)
        cleanHeightConstraints(item: items[3], itemHeight: &secondCommentaryTextHeight)
    }
    
    func cleanHeightConstraints(item: UIView, itemHeight: inout CGFloat){
        for constraint in item.constraints {
            if (constraint.firstAttribute == .height) {
                constraint.isActive = false;
            }
        }
        if(item.frame.height == 0){
            let heightConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: itemHeight)
            NSLayoutConstraint.activate([heightConstraint])
        }else{
            itemHeight = item.frame.height
        }
    }
    
    func calculateTextHeight(item: UIView, textStr:String, clear: Bool, validateHeightFrame: Bool){
        let prototypeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 16, height: CGFloat.greatestFiniteMagnitude))
        prototypeLabel.text = textStr
        prototypeLabel.numberOfLines = 3
        prototypeLabel.lineBreakMode = .byWordWrapping
        prototypeLabel.font = UIFont(name: "Regular", size: 15.0)
        prototypeLabel.sizeToFit()
        let protHeight = prototypeLabel.frame.height
        
        if(clear){
            for constraint in item.constraints {
                if (constraint.firstAttribute == .height) {
                    constraint.isActive = false;
                }
            }
            if(validateHeightFrame){
                if(item.frame.height == 0){
                    let heightConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: protHeight)
                    NSLayoutConstraint.activate([heightConstraint])
                }
            }else{
                let heightConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: protHeight)
                NSLayoutConstraint.activate([heightConstraint])
            }
        }
    }
    
    func showOptions (sender: UIButton!){
        
        let popover = storyboard?.instantiateViewController(withIdentifier: "OptionsPopoverViewController") as! OptionsPopoverViewController
        popover.modalPresentationStyle = .popover
        popover.popoverPresentationController?.delegate = self
        popover.popoverPresentationController?.sourceView = sender
        popover.popoverPresentationController?.sourceRect = sender.bounds
        popover.popoverPresentationController?.permittedArrowDirections = .up
        popover.preferredContentSize = CGSize(width:UIScreen.main.bounds.width, height: 64)
        popover.contentId = sender.tag
        
        self.present(popover, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func showFullscreen(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as? UIImageView
        let imageResize = resizeToScreenSize(image: (imageView?.image!)!)
        let newImageView = UIImageView(image: imageResize)
        newImageView.frame = self.view.frame
        newImageView.alpha = 0
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        newImageView.fadeIn(withDuration: 0.8)
    }
    
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let tappedImage = sender.view as! UIImageView
        tappedImage.fadeOut(withDuration: 0.8)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        
        image.draw(in: CGRect(x: 0, y: 0,width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func resizeToScreenSize(image: UIImage)->UIImage{
        
        let screenSize = self.view.bounds.size
        return resizeImage(image: image, newWidth: screenSize.width)
    }
    
    func likeContent(sender: UIButton){
        /*
         UserContentTypeId
         1->Like
         2->Guardar
         3->Reportar
         4->Compartir
         5->Chat
         */
        
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId":userId,
            "ContentId":sender.tag,
            "UserShareId":"",
            "UserContentTypeId":"1",
            "ReportTypeId":"",
            "ReportDescription":"",
            "Viewed":""
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/SaveUserContent", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if(dataResp["result"] as! Bool){
                                if let itemTable = self.publicationArray.filter({$0.id == sender.tag}).first
                                {
                                    if(sender.accessibilityHint == "notLike"){
                                        sender.setImage(UIImage(named: "like_icon.png"), for: .normal)
                                        itemTable.isLike = true
                                        sender.accessibilityHint = "like"
                                    }else{
                                        sender.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                                        itemTable.isLike = false
                                        sender.accessibilityHint = "notLike"
                                    }
                                }
                            }
                        }else{
                            Utils().createAlert("Presentamos inconvenientes con el servicio de guardar contenido. Por favor intetelo mas tarde.", self)
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de guardar contenido. Por favor intetelo mas tarde.", self)
                }
            }
        }
    }
    
    func commentContent(sender: UIButton){
        goCommentView(sender.tag)
    }
    
    func commentContentFromText(sender: UITapGestureRecognizer) {
        if let uiLabel = (sender.view as? UILabel){
            goCommentView(uiLabel.tag)
        }
    }
    
    func goCommentView(_ tag:Int){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CommentPublicationViewController") as! CommentPublicationViewController
        nextViewController.publicationId = tag
        nextViewController.userName = user.name
        nextViewController.pictureProfile = user.pictureProfile
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func sharePublication(sender: UIButton){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ShowPublicationViewController") as! ShowPublicationViewController
        nextViewController.contentType = sender.tag
        nextViewController.strData = sender.accessibilityHint!
        nextViewController.picProfileStr = user.name
        nextViewController.userNameStr = user.pictureProfile
        self.present(nextViewController, animated:true, completion:nil)
    }

    @IBAction func backContent(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
