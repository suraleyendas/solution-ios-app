//
//  AwardViewController.swift
//  LegendService
//
//  Created by informatica on 13/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class AwardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPoints: UILabel!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var dataArray = [Award]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataArray.append(Award.init(id: 1, image: "award_1.jpg", title: "50% de descuento", subtitle: "en una docena de dunkin' donuts", points: 500))
        dataArray.append(Award.init(id: 2, image: "award_2.jpg", title: "20% de descuento", subtitle: "en un combo Big Mac", points: 340))
        dataArray.append(Award.init(id: 3, image: "award_3.png", title: "10% de descuento", subtitle: "una mega barra de chocolate con mas de 12 sabores diferentes y posibilidad de aumentar el número de sabores", points: 1200))
        
        //Picture
        userImage.layer.borderWidth = 4
        userImage.layer.masksToBounds = true
        userImage.layer.borderColor = UIColor.lightGray.cgColor
        userImage.clipsToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //loadUserData()
    }
    
    func loadUserData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUser/\(userId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            self.loadPictureProfile((response["Picture"] as? String)!);
                            self.userName.text = (response["Name"] as? String)!
                        }else{
                            Utils().createDissmisAlert("Presentamos inconvenientes con el servicio al recuperar usuario. Por favor intetelo mas tarde.", self)
                        }
                        self.stopAnimating()
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadUserData);
                }
            }
        }
    }
    
    func loadPictureProfile(_ imageUrlString : String){
        let imageUrl:URL = URL(string: imageUrlString)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        self.userImage.image = image
    }
    
    override func viewWillLayoutSubviews() {
        //Border image
        userImage.layer.cornerRadius = min(userImage.bounds.size.height, userImage.bounds.size.width)/2.0
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "awardCell", for: indexPath) as! AwardTableViewCell
        
        let award = dataArray[indexPath.row]
        cell.wImage.clipsToBounds = true
        cell.wImage.image = UIImage(named: award.image)
        cell.wTitle.text = award.title
        cell.wSubtitle.text = award.subtitle
        cell.wPoints.text = "\(award.points) pts."
        
        cell.wRedeemBtn.addTarget(self, action: #selector(goConfirmInfo), for: .touchUpInside)
        
        return cell
    }
    
    func goConfirmInfo(sender: UIButton){

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ConfirmInfoViewController") as! ConfirmInfoViewController
        nextViewController.modalPresentationStyle = .overCurrentContext
        self.present(nextViewController, animated: false, completion: nil)
    }
}
