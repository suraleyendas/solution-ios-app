//
//  ConfirmVoteViewController.swift
//  LegendService
//
//  Created by informatica on 20/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ConfirmVoteViewController: UIViewController, UITextViewDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var awardsItemBar: UITabBarItem!
    @IBOutlet weak var pictureProfileVote: UIImageView!
    @IBOutlet weak var reasonForVote: UITextView!
    @IBOutlet weak var confirmVoteBtn: UIButton!
    @IBOutlet weak var voteName: UILabel!
    @IBOutlet weak var voteEmail: UILabel!
    @IBOutlet weak var companyCityVote: UILabel!
    
    var userToVoteId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Up view
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Load Data
        loadData()
        
        //Set image properties
        pictureProfileVote.layer.borderWidth = 8
        pictureProfileVote.layer.masksToBounds = true
        pictureProfileVote.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfileVote.clipsToBounds = true
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        reasonForVote.layer.borderColor = myColor.cgColor
        reasonForVote.layer.borderWidth = 1.0
        reasonForVote.layer.cornerRadius = 5.0
        
        //Hide border tab bar
        tabBar.layer.borderWidth = 0.50
        tabBar.layer.borderColor = UIColor.clear.cgColor
        tabBar.clipsToBounds = true
        
        //Set selected awards item
        tabBar.selectedItem = tabBar.items![2]
        
        // remove default border
        tabBar.frame.size.width = self.view.frame.width + 4
        tabBar.frame.origin.x = -2
        
        //Hide keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ConfirmVoteViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        if(textView.text! != ""){
            confirmVoteBtn.isEnabled = true
        }else{
            confirmVoteBtn.isEnabled = false
        }
    }
    
    func loadData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUser/\(userToVoteId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            let picPrif = (response["Picture"] as? String)!
                            if(picPrif != ""){
                                self.loadPictureProfile(picPrif);
                            }else{
                                let image : UIImage = UIImage(named:"big_pic_prof.jpg")!
                                self.pictureProfileVote.image = image
                            }
                            self.voteName.text = (response["Name"] as? String)!
                            self.voteEmail.text = (response["Email"] as? String)!
                            self.companyCityVote.text = "\((response["Company"] as? String)!) - \((response["City"] as? String)!)"
                            self.stopAnimating()
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }
    }

    func loadPictureProfile(_ imageUrlString : String){
        let imageUrl:URL = URL(string: imageUrlString)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        self.pictureProfileVote.image = image
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        pictureProfileVote.layer.cornerRadius = min(pictureProfileVote.bounds.size.height, pictureProfileVote.bounds.size.width)/2.0
        
        // set color as selected background color
        let width = tabBar.bounds.width
        var selectionImage = UIImage(named:"item_selected_background.png")
        let tabSize = CGSize(width: width/4, height: tabBar.bounds.height)
        
        UIGraphicsBeginImageContext(tabSize)
        selectionImage?.draw(in: CGRect(x: 0, y: 0, width: tabSize.width, height: tabSize.height))
        selectionImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        tabBar.selectionIndicatorImage = selectionImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func confirmVote(_ sender: Any) {
        toVote()
    }
    
    func toVote(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "userId": userId,
            "userIdVote": userToVoteId,
            "description": reasonForVote.text,
            "isActive": true
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "/UsersAPI/SaveUserVote", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        self.dismiss(animated: true, completion: nil)
                    });
                    self.stopAnimating()
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.toVote);
                }
            }
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func backConfirmVote(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
