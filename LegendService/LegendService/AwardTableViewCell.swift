//
//  AwardTableViewCell.swift
//  LegendService
//
//  Created by informatica on 13/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class AwardTableViewCell: UITableViewCell {

    @IBOutlet weak var wImage: UIImageView!
    @IBOutlet weak var wTitle: UILabel!
    @IBOutlet weak var wSubtitle: UILabel!
    @IBOutlet weak var wPoints: UILabel!
    @IBOutlet weak var wRedeemBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func goConfirmInfo(_ sender: Any) {
        
    }
}
