//
//  Event.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 3/1/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class Event: NSObject {
    
    var userId = 0
    var userName = ""
    var userPicture = ""
    var id = 0
    var publishDate = ""
    var filePath = ""
    var text = ""
    var isVieved = false
    
    init(userId:Int, userName: String, userPicture: String, id: Int, publishDate: String, filePath: String, text: String, isVieved: Bool){
        self.userId = userId
        self.userName = userName
        self.userPicture = userPicture
        self.id = id
        self.publishDate = publishDate
        self.filePath = filePath
        self.text = text
        self.isVieved = isVieved
    }
}
