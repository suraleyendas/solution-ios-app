//
//  FollowerUserTableViewCell.swift
//  LegendService
//
//  Created by informatica on 20/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class FollowerUserTableViewCell: UITableViewCell {

    @IBOutlet weak var followerImage: UIImageView!
    @IBOutlet weak var followerName: UILabel!
    @IBOutlet weak var followerEmail: UILabel!
    @IBOutlet weak var followerBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        followerImage.layer.borderWidth = 2
        followerImage.layer.masksToBounds = false
        followerImage.layer.borderColor = UIColor.lightGray.cgColor
        followerImage.layer.cornerRadius = min(followerImage.frame.height, followerImage.frame.width)/2.0
        followerImage.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
