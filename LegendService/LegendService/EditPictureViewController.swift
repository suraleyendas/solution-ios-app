//
//  EditPictureViewController.swift
//  LegendService
//
//  Created by informatica on 15/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class EditPictureViewController: UIViewController, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    var showView = true
    
    var imageView = UIImageView()
    var action = Int()
    var returnImage = UIImage()
    var fromTakePicture = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        scrollView.delegate = self
        
        imageView.frame = CGRect(x: 0, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
        imageView.image = UIImage(named: "image_sl")
        imageView.isUserInteractionEnabled = true
        
        scrollView.addSubview(imageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Add rect shadow
        if(imageView.image != nil){
            let length = UIScreen.main.bounds.width
            let maskLength = UIScreen.main.bounds.width * 0.18
            let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: length, height: maskLength), cornerRadius: 0)
            let circlePath = UIBezierPath(roundedRect: CGRect(x: 0, y: contentView.bounds.height - maskLength, width:length, height: maskLength), cornerRadius: 0)
            path.append(circlePath)
            path.usesEvenOddFillRule = true
            
            let fillLayer = CAShapeLayer()
            fillLayer.path = path.cgPath
            fillLayer.fillRule = kCAFillRuleEvenOdd
            fillLayer.fillColor = UIColor.gray.cgColor
            fillLayer.opacity = 0.5
            contentView.layer.addSublayer(fillLayer)
        }
    
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if(action == 1){
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }else{
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        }
        if(showView){
            showView = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String : AnyObject]){
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
        imageView.contentMode = UIViewContentMode.center
        imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        scrollView.contentSize = image.size
        
        let scrollViewFrame = scrollView.frame
        let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
        let minScale = min(scaleHeight, scaleWidth)
        
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 1
        scrollView.zoomScale = minScale
        
        centerScrollViewContents()
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: false, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func centerScrollViewContents(){
        let boundsSize = scrollView.bounds.size
        var contentsFrame = imageView.frame
        
        if contentsFrame.size.width < boundsSize.width{
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2
        }else{
            contentsFrame.origin.x = 0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2
        }else{
            contentsFrame.origin.y = 0
        }
        
        imageView.frame = contentsFrame
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerScrollViewContents()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    
    @IBAction func cropAndSave(_ sender: AnyObject) {
        
        let sizeNewImage : CGSize = scrollView.bounds.size
        var sizeNewImageLessBorder : CGSize = CGSize()
        sizeNewImageLessBorder.height = sizeNewImage.height - 2*(sizeNewImage.height * 0.18)
        sizeNewImageLessBorder.width = sizeNewImage.width
        UIGraphicsBeginImageContextWithOptions(sizeNewImageLessBorder, true, UIScreen.main.scale)
        let offset = scrollView.contentOffset
        
        UIGraphicsGetCurrentContext()!.translateBy(x: -offset.x, y: -(offset.y + offset.y * 0.18))
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        returnImage = (image?.resized(withPercentage: 0.8)!)!
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
