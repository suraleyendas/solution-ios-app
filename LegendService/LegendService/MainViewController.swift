//
//  MainViewController.swift
//  LegendService
//
//  Created by informatica on 22/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverPresentationControllerDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var pictureView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var expressView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pictureProfileMain: UIImageView!
    @IBOutlet weak var userNameMain: UILabel!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var publicationArray = [Publication]()
    var pictureProf = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 600
        
        //Picture
        pictureProfileMain.layer.borderWidth = 4
        pictureProfileMain.layer.masksToBounds = true
        pictureProfileMain.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfileMain.clipsToBounds = true
        
        loadUserData()
        
        //From background
        NotificationCenter.default.addObserver(self, selector:#selector(loadTableData), name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(forName: .UIContentSizeCategoryDidChange, object: .none, queue: OperationQueue.main) { [weak self] _ in
            self?.tableView.reloadData()
        }
        
        //Add right border to top bar buttons
        let rightBorderPhoto = UIView()
        rightBorderPhoto.backgroundColor = UIColor.lightGray
        rightBorderPhoto.alpha = 0.5
        rightBorderPhoto.frame = CGRect(x:pictureView.bounds.size.width - 2.0, y:3, width:2.0, height:pictureView.bounds.size.height - 3);
        pictureView.addSubview(rightBorderPhoto)
        let rightBorderVideo = UIView()
        rightBorderVideo.backgroundColor = UIColor.lightGray
        rightBorderVideo.frame = CGRect(x:videoView.bounds.size.width - 1.0, y:3, width:1.0, height:videoView.bounds.size.height - 3);
        videoView.addSubview(rightBorderVideo)
        let rightBorderEvent = UIView()
        rightBorderEvent.backgroundColor = UIColor.lightGray
        rightBorderEvent.frame = CGRect(x:eventView.bounds.size.width - 1.0, y:3, width:1.0, height:eventView.bounds.size.height - 3);
        eventView.addSubview(rightBorderEvent)
        
        loadTableData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        //Border image
        pictureProfileMain.layer.cornerRadius = min(pictureProfileMain.bounds.size.height, pictureProfileMain.bounds.size.width)/2.0
    }
    
    func loadUserData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUser/\(userId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            self.pictureProf = (response["Picture"] as? String)!
                            self.loadPictureProfile(self.pictureProf);
                            self.userNameMain.text = (response["Name"] as? String)!
                        }else{
                            Utils().createDissmisAlert("Presentamos inconvenientes con el servicio al recuperar usuario. Por favor intetelo mas tarde.", self)
                        }
                        self.stopAnimating()
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadUserData);
                }
            }
        }
    }
    
    func loadTableData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId":userId,
            "TopComment":"2",
            "PageIndex": 0,
            "PageSize": 100
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/GetWallByUser", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let isEmptyResult = returnData["empty"] as? Bool {
                            if(!isEmptyResult){
                                if let respData = returnData["data"] as? [String: Any] {
                                    if let userPublicationArray = respData["arrayContent"] as? [[String: Any]] {
                                        self.publicationArray = [Publication]()
                                        
                                        for userPublication in userPublicationArray{
                                            
                                            var commentaries = [Commentary]()
                                            let publicationCommentaries = userPublication["ListComment"] as? [[String: Any]]
                                            for publicationCommentary in publicationCommentaries!{
                                                commentaries.append(
                                                    Commentary.init(
                                                        commentParentid: (publicationCommentary["ContentParentId"] as? Int)!,
                                                        userId: (publicationCommentary["UserId"] as? Int)!,
                                                        userImage: (publicationCommentary["Picture"] as? String)!,
                                                        userName: (publicationCommentary["FullName"] as? String)!,
                                                        text: (publicationCommentary["StrComment"] as? String)!
                                                    )
                                                )
                                            }
                                            
                                            self.publicationArray.append(
                                                Publication.init(
                                                    id: (userPublication["ContentId"] as? Int)!,
                                                    userId: (userPublication["UserId"] as? Int)!,
                                                    filePath: (userPublication["FilePath"] as? String)!,
                                                    userCommentary: (userPublication["strContent"] as? String)!,
                                                    isActive: (userPublication["IsActive"] as? Bool)!,
                                                    contentTypeId: (userPublication["ContentTypeId"] as? Int)!,
                                                    contentType: (userPublication["ContentType"] as? String)!,
                                                    UserContentTypeId: (userPublication["UserContentTypeId"] as? Int)!,
                                                    UserContentType: (userPublication["UserContentType"] as? String)!,
                                                    isLike: (userPublication["IsLike"] as? Bool)!,
                                                    countLike: (userPublication["CountLike"] as? Int)!,
                                                    countComment: (userPublication["CountComment"] as? Int)!,
                                                    picture: (userPublication["Picture"] as? String)!,
                                                    name: (userPublication["Name"] as? String)!,
                                                    commetaries: commentaries
                                                )
                                            )
                                        }
                                        self.tableView.reloadData()
                                        self.stopAnimating()
                                    }
                                }
                            }else{
                                self.stopAnimating()
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadTableData);
                }
            }
        }
    }
    
    func loadPictureProfile(_ imageUrlString : String){
        let imageUrl:URL = URL(string: imageUrlString)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        self.pictureProfileMain.image = image
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return publicationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let publication = publicationArray[indexPath.row]
        
        switch publication.contentTypeId {
            //Text
            case 1:
                if(publication.countComment == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textWithoutCommentaries", for: indexPath) as! PrototypeSevenTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.textContent.text = publication.userCommentary
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.textContent.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.textContent.tag = publication.id
                    cell.textContent.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.userCommentary
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    //show options
                    cell.optionsBtn.tag = publication.id
                    cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
                    
                    return cell
                }else if(publication.countComment == 1){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textOneCommentaries", for: indexPath) as! PrototypeEightTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.textContent.text = publication.userCommentary
                    
                    //Add bottom border options bar
                    let bottomBar = UIView()
                    bottomBar.backgroundColor = UIColor.lightGray
                    bottomBar.frame = CGRect(x:0, y:pictureView.bounds.size.height, width:pictureView.bounds.size.height, height:1.0);
                    cell.optionsBar.addSubview(bottomBar)
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.textContent.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.textContent.tag = publication.id
                    cell.textContent.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.userCommentary
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    //show options
                    cell.optionsBtn.tag = publication.id
                    cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
                    
                    let commentary = publication.commetaries[0]
                    cell.commentaryUserName.text = commentary.userName
                    cell.commentaryText.text = commentary.text
                    if(commentary.userImage != ""){
                        cell.commentaryPictureProfile.image = Utils().decodeImageToBase64(commentary.userImage)
                    }else{
                        cell.commentaryPictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textTwoCommentaries", for: indexPath) as! PrototypeNineTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.textContent.text = publication.userCommentary
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.textContent.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.textContent.tag = publication.id
                    cell.textContent.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.userCommentary
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    //show options
                    cell.optionsBtn.tag = publication.id
                    cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)

                    
                    let firstCommentary = publication.commetaries[0]
                    cell.firstCommentaryUser.text = firstCommentary.userName
                    cell.firstCommentaryText.text = firstCommentary.text
                    
                    if(firstCommentary.userImage != ""){
                        cell.firstCommentaryPicture.image = Utils().decodeImageToBase64(firstCommentary.userImage)
                    }else{
                        cell.firstCommentaryPicture.image = UIImage(named:"small_pic_prof.png")!
                    }
                    
                    let secondCommentary = publication.commetaries[1]
                    cell.secondCommentaryUser.text = secondCommentary.userName
                    cell.secondCommentaryText.text = secondCommentary.text
                    if(secondCommentary.userImage != ""){
                        cell.secondCommentaryPicture.image = Utils().decodeImageToBase64(secondCommentary.userImage)
                    }else{
                        cell.secondCommentaryPicture.image = UIImage(named:"small_pic_prof.png")!
                    }
                    
                    return cell
                }
            //Video
            case 2:
                if(publication.countComment == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "videoWithoutCommentaries", for: indexPath) as! PrototypeFourTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.userCommentary.text = publication.userCommentary
                    let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"\(publication.filePath)\" ></iframe>"
                    cell.videoContent.loadHTMLString(htmlStr, baseURL: nil)
                    cell.videoContent.scrollView.isScrollEnabled = false
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.userCommentary.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.userCommentary.tag = publication.id
                    cell.userCommentary.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.filePath
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    //show options
                    cell.optionsBtn.tag = publication.id
                    cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
                    
                    return cell
                }else if(publication.countComment == 1){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "videoOneCommentaries", for: indexPath) as! PrototypeFiveTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.userCommentary.text = publication.userCommentary
                    let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"\(publication.filePath)\" ></iframe>"
                    cell.videoContent.loadHTMLString(htmlStr, baseURL: nil)
                    cell.videoContent.scrollView.isScrollEnabled = false
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.userCommentary.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.userCommentary.tag = publication.id
                    cell.userCommentary.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.filePath
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    //show options
                    cell.optionsBtn.tag = publication.id
                    cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
                    
                    let commentary = publication.commetaries[0]
                    if(commentary.userImage != ""){
                        cell.commentaryPicture.image = Utils().decodeImageToBase64(commentary.userImage)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.commentaryName.text = commentary.userName
                    cell.commentaryText.text = commentary.text
                    cell.commentaryPicture.layer.cornerRadius = min(cell.commentaryPicture.bounds.height, cell.commentaryPicture.bounds.width)/2.0
                    cell.commentaryPicture.clipsToBounds = true
                    
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "videoTwoCommentaries", for: indexPath) as! PrototypeSixTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.userCommentary.text = publication.userCommentary
                    let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"\(publication.filePath)\" ></iframe>"
                    cell.videoContent.loadHTMLString(htmlStr, baseURL: nil)
                    cell.videoContent.scrollView.isScrollEnabled = false
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.userCommentary.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.userCommentary.tag = publication.id
                    cell.userCommentary.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.filePath
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    let firstCommentary = publication.commetaries[0]
                    cell.firstCommentaryUser.text = firstCommentary.userName
                    cell.firstCommentaryText.text = firstCommentary.text
                    
                    if(firstCommentary.userImage != ""){
                        cell.firstCommentaryPicture.image = Utils().decodeImageToBase64(firstCommentary.userImage)
                    }else{
                        cell.firstCommentaryPicture.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.firstCommentaryPicture.layer.cornerRadius = min(cell.firstCommentaryPicture.bounds.height, cell.firstCommentaryPicture.bounds.width)/2.0
                    cell.firstCommentaryPicture.clipsToBounds = true
                    
                    let secondCommentary = publication.commetaries[1]
                    cell.secondCommentaryUser.text = secondCommentary.userName
                    cell.secondCommentaryText.text = secondCommentary.text
                    if(secondCommentary.userImage != ""){
                        cell.secondCommentaryPicture.image = Utils().decodeImageToBase64(secondCommentary.userImage)
                    }else{
                        cell.secondCommentaryPicture.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.secondCommentaryPicture.layer.cornerRadius = min(cell.secondCommentaryPicture.bounds.height, cell.secondCommentaryPicture.bounds.width)/2.0
                    cell.secondCommentaryPicture.clipsToBounds = true
                    cell.secondCommentaryPicture.layer.cornerRadius = min(cell.secondCommentaryPicture.bounds.height, cell.secondCommentaryPicture.bounds.width)/2.0
                    cell.secondCommentaryPicture.clipsToBounds = true
                    
                    return cell
                }
            //Image
            case 3:
                if(publication.countComment == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "imageWithoutCommentaries", for: indexPath) as! PrototypeOneTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.userCommentary.text = publication.userCommentary
                    cell.imageContent.imageFromServerURL(urlString: publication.filePath)
                    cell.imageContent.clipsToBounds = true;
                    cell.imageContent.isUserInteractionEnabled = true
                    
                    let tapShow = UITapGestureRecognizer(target: self, action: #selector(showFullscreen))
                    cell.imageContent.addGestureRecognizer(tapShow)
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.userCommentary.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.userCommentary.tag = publication.id
                    cell.userCommentary.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.filePath
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    //show options
                    cell.optionsBtn.tag = publication.id
                    cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
                    
                    return cell
                }else if(publication.countComment == 1){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "imageOneCommentaries", for: indexPath) as! PrototypeTwoTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.userCommentary.text = publication.userCommentary
                    cell.imageContent.imageFromServerURL(urlString: publication.filePath)
                    cell.imageContent.clipsToBounds = true;
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.userCommentary.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.userCommentary.tag = publication.id
                    cell.userCommentary.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.filePath
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    //show options
                    cell.optionsBtn.tag = publication.id
                    cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
                    
                    let commentary = publication.commetaries[0]
                    if(commentary.userImage != ""){
                        cell.commentPicture.image = Utils().decodeImageToBase64(commentary.userImage)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.commentName.text = commentary.userName
                    cell.commentText.text = commentary.text
                    cell.commentPicture.layer.cornerRadius = min(cell.commentPicture.bounds.height, cell.commentPicture.bounds.width)/2.0
                    cell.commentPicture.clipsToBounds = true
                    
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "imageTwoCommentaries", for: indexPath) as! PrototypeThreeTableViewCell
                    if(publication.picture != ""){
                        cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                    }else{
                        cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.userName.text = publication.name
                    cell.userCommentary.text = publication.userCommentary
                    cell.imageContent.imageFromServerURL(urlString: publication.filePath)
                    cell.imageContent.clipsToBounds = true;
                    
                    //Update counters
                    cell.likeCounter.text = "\(publication.countLike)"
                    cell.commentaryCounter.text = "\(publication.countComment)"
                    
                    //Like
                    cell.likeBtn.tag = publication.id
                    if(publication.isLike){
                        cell.likeBtn.accessibilityHint = "like"
                        cell.likeBtn.setImage(UIImage(named: "like_icon.png"), for: .normal)
                    }else{
                        cell.likeBtn.accessibilityHint = "notLike"
                        cell.likeBtn.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                    }
                    cell.likeBtn.addTarget(self, action: #selector(likeContent), for: .touchUpInside)
                    
                    //Comment
                    cell.commentaryBtn.tag = publication.id
                    cell.commentaryBtn.accessibilityHint = "\(publication.name)$\(publication.picture)"
                    if(publication.commetaries.count > 0){
                        cell.commentaryBtn.setImage(UIImage(named: "comment_icon.png"), for: .normal)
                    }else{
                        cell.commentaryBtn.setImage(UIImage(named: "not_comment_icon.png"), for: .normal)
                    }
                    cell.commentaryBtn.addTarget(self, action: #selector(commentContent), for: .touchUpInside)
                    
                    //Text gesture
                    cell.userCommentary.numberOfLines = 3
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(commentContent))
                    cell.userCommentary.tag = publication.id
                    cell.userCommentary.addGestureRecognizer(tapGesture)
                    
                    //Share
                    cell.shareBtn.tag = publication.contentTypeId
                    cell.shareBtn.accessibilityHint = publication.filePath
                    cell.shareBtn.addTarget(self, action: #selector(sharePublication), for: .touchUpInside)
                    
                    let firstCommentary = publication.commetaries[0]
                    cell.firstCommentaryUser.text = firstCommentary.userName
                    cell.firstCommentaryText.text = firstCommentary.text
                    
                    if(firstCommentary.userImage != ""){
                        cell.firstCommentaryPicture.image = Utils().decodeImageToBase64(firstCommentary.userImage)
                    }else{
                        cell.firstCommentaryPicture.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.firstCommentaryPicture.layer.cornerRadius = min(cell.firstCommentaryPicture.bounds.height, cell.firstCommentaryPicture.bounds.width)/2.0
                    cell.firstCommentaryPicture.clipsToBounds = true
                    
                    let secondCommentary = publication.commetaries[1]
                    cell.secondCommentaryUser.text = secondCommentary.userName
                    cell.secondCommentaryText.text = secondCommentary.text
                    if(secondCommentary.userImage != ""){
                        cell.secondCommentaryPicture.image = Utils().decodeImageToBase64(secondCommentary.userImage)
                    }else{
                        cell.secondCommentaryPicture.image = UIImage(named:"small_pic_prof.png")!
                    }
                    cell.secondCommentaryPicture.layer.cornerRadius = min(cell.secondCommentaryPicture.bounds.height, cell.secondCommentaryPicture.bounds.width)/2.0
                    cell.secondCommentaryPicture.clipsToBounds = true
                    
                    return cell
                }
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "textWithoutCommentaries", for: indexPath) as! PrototypeSevenTableViewCell
                if(publication.picture != ""){
                    cell.pictureProfile.image = Utils().decodeImageToBase64(publication.picture)
                }else{
                    cell.pictureProfile.image = UIImage(named:"small_pic_prof.png")!
                }
                cell.userName.text = publication.name
                cell.textContent.text = publication.userCommentary
                
                //show options
                cell.optionsBtn.tag = publication.id
                cell.optionsBtn.addTarget(self, action: #selector(showOptions), for: .touchUpInside)
            return cell
        }
    }
    
    func showOptions (sender: UIButton!){
        
        let popover = storyboard?.instantiateViewController(withIdentifier: "OptionsPopoverViewController") as! OptionsPopoverViewController
        popover.modalPresentationStyle = .popover
        popover.popoverPresentationController?.delegate = self
        popover.popoverPresentationController?.sourceView = sender
        popover.popoverPresentationController?.sourceRect = sender.bounds
        popover.popoverPresentationController?.permittedArrowDirections = .up
        popover.preferredContentSize = CGSize(width:UIScreen.main.bounds.width, height: 64)
        popover.contentId = sender.tag
        
        self.present(popover, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func showFullscreen(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as? UIImageView
        let imageResize = resizeToScreenSize(image: (imageView?.image!)!)
        let newImageView = UIImageView(image: imageResize)
        newImageView.frame = self.view.frame
        newImageView.alpha = 0
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        newImageView.fadeIn(withDuration: 0.8)
    }
    
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let tappedImage = sender.view as! UIImageView
        tappedImage.fadeOut(withDuration: 0.8)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        
        image.draw(in: CGRect(x: 0, y: 0,width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func resizeToScreenSize(image: UIImage)->UIImage{
        
        let screenSize = self.view.bounds.size
        return resizeImage(image: image, newWidth: screenSize.width)
    }
    
    func likeContent(sender: UIButton){
        /*
         UserContentTypeId
         1->Like
         2->Guardar
         3->Reportar
         4->Compartir
         5->Chat
         */
        
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId":userId,
            "ContentId":sender.tag,
            "UserShareId":"",
            "UserContentTypeId":"1",
            "ReportTypeId":"",
            "ReportDescription":"",
            "Viewed":""
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/SaveUserContent", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if(dataResp["Result"] as! Bool){
                                if let itemTable = self.publicationArray.filter({$0.id == sender.tag}).first
                                {
                                    if(sender.accessibilityHint == "notLike"){
                                        sender.setImage(UIImage(named: "like_icon.png"), for: .normal)
                                        itemTable.isLike = true
                                        sender.accessibilityHint = "like"
                                    }else{
                                        sender.setImage(UIImage(named: "not_like_icon.png"), for: .normal)
                                        itemTable.isLike = false
                                        sender.accessibilityHint = "notLike"
                                    }
                                }
                            }
                        }else{
                            Utils().createAlert("Presentamos inconvenientes con el servicio de guardar contenido. Por favor intetelo mas tarde.", self)
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de guardar contenido. Por favor intetelo mas tarde.", self)
                }
            }
        }
    }
    
    func commentContent(sender: UIButton){
        goCommentView(sender.tag)
    }
    
    func commentContentFromText(sender: UITapGestureRecognizer) {
        if let uiLabel = (sender.view as? UILabel){
            goCommentView(uiLabel.tag)
        }
    }
    
    func goCommentView(_ tag:Int){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CommentPublicationViewController") as! CommentPublicationViewController
        nextViewController.publicationId = tag
        nextViewController.userName = userNameMain.text!
        nextViewController.pictureProfile = pictureProf
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func sharePublication(sender: UIButton){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ShowPublicationViewController") as! ShowPublicationViewController
        nextViewController.contentType = sender.tag
        nextViewController.strData = sender.accessibilityHint!
        nextViewController.picProfileStr = pictureProf
        nextViewController.userNameStr = userNameMain.text!
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func goExpressYourself(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExpressYourselfViewController") as! ExpressYourselfViewController
        nextViewController.image = pictureProfileMain.image!
        nextViewController.name = userNameMain.text!
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    @IBAction func goPublishPhoto(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PublishPhotoViewController") as! PublishPhotoViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    @IBAction func goPublishVideo(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PublishVideoViewController") as! PublishVideoViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    @IBAction func goEvents(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EventViewController") as! EventViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    @IBAction func goProfile(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        nextViewController.isFromMain = true
        self.present(nextViewController, animated: true, completion: nil)
    }
    @IBAction func goMessages(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
}

extension UIImageView {
    public func imageFromServerURL(urlString: String) {
        //let urlString = "https://lh4.googleusercontent.com/eHuqFM1kf010LnscLCvuEWjworpb1TO7-1CLwcIXsmDb-0yxqWwOHFDzBKHal056Ughq3anz1ipCutjacHQ2Mc3dQQ67D37FN2VnvobKK53q5e2SAhw4CKsWIMT-UgyuDdc2e4o"
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
}
