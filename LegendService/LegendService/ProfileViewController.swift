//
//  EditProfileViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/14/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var pictureProfile: UIImageView!
    @IBOutlet weak var fullUserNameLabel: UILabel!
    @IBOutlet weak var citiContentView: UIView!
    @IBOutlet weak var companyContentView: UIView!
    @IBOutlet weak var citiesCombo: UITextField!
    @IBOutlet weak var companyCombo: UITextField!
    @IBOutlet weak var emailServerCombo: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var editpictureProfileMask: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    let viewOnePicker = UIPickerView()
    let viewTwoPicker = UIPickerView()
    let viewThreePicker = UIPickerView()
    let viewFourPicker = UIPickerView()
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    var keyboardHeight = CGFloat()
    
    var fromProfile = false
    
    var pickerShow = 0;
    var cities = [String]()
    var citiesId = [Int]()
    var companies = [String]()
    var companiesId = [Int]()
    var emailServers = [String]()
    var emailServersId = [Int]()
    var moveView = false
    var keyboardSizeLength : CGFloat = 0
    
    var pictureOptions = ["Cámara", "Galería", "Cancelar"]
    var pictureOptionIds = [0, 1, 2]
    var pictureOptionChoosed = 0
    var imagePicker:UIImagePickerController! = UIImagePickerController()
    var editPictureViewController = EditPictureViewController()
    
    var isCitiesLoaded = false
    var isCompaniesLoaded = false
    var isEmailServersLoaded =  false
    var citiSelected = 0
    var companySelected = 0
    var emailServersSelected = 0
    var isDataLoaded = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(fromProfile){
            nextButton.setImage(UIImage(named: "update_btn.png") as UIImage?, for: .normal)
        }else{
            let userDefaults = UserDefaults.standard
            userDefaults.set(3, forKey: "root_view")
        }
        
        editPictureViewController = storyBoard.instantiateViewController(withIdentifier: "EditPictureViewController") as! EditPictureViewController
        editPictureViewController.returnImage = UIImage()
        editPictureViewController.fromTakePicture = false
        
        emailText.delegate = self
        emailText.layer.borderColor = UIColor.lightGray.cgColor
        emailText.layer.borderWidth = 1.0;
        emailText.layer.cornerRadius = 5.0;
        editpictureProfileMask.tintColor = UIColor.clear
        
        //Picture
        pictureProfile.layer.borderWidth = 8
        pictureProfile.layer.masksToBounds = true
        pictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfile.clipsToBounds = true
        
        //Boxes
        citiContentView.layer.cornerRadius = 5
        citiContentView.layer.borderColor = UIColor.lightGray.cgColor
        citiContentView.layer.borderWidth = 1
        companyContentView.layer.cornerRadius = 5
        companyContentView.layer.borderColor = UIColor.lightGray.cgColor
        companyContentView.layer.borderWidth = 1
        emailServerCombo.layer.cornerRadius = 5
        emailServerCombo.layer.borderColor = UIColor.lightGray.cgColor
        emailServerCombo.layer.borderWidth = 1
        
        //ComboBoxes
        addBtnPicker()
        viewOnePicker.delegate = self
        viewOnePicker.dataSource = self
        citiesCombo.inputView = viewOnePicker
        viewTwoPicker.delegate = self
        viewTwoPicker.dataSource = self
        companyCombo.inputView = viewTwoPicker
        viewThreePicker.delegate = self
        viewThreePicker.dataSource = self
        emailServerCombo.inputView = viewThreePicker
        viewFourPicker.delegate = self
        viewFourPicker.dataSource = self
        editpictureProfileMask.inputView = viewFourPicker
        
        //Up view
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Hide keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //From background
        NotificationCenter.default.addObserver(self, selector:#selector(loadData), name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadCities()
        if(editPictureViewController.returnImage.size.height != 0){
            pictureProfile.image = editPictureViewController.returnImage
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
             pictureProfile.layer.cornerRadius = min(pictureProfile.bounds.size.height, pictureProfile.bounds.size.width)/2.0
    }
    
    func loadData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUser/\(userId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            if(self.fromProfile){
                                self.citiesCombo.text = response["City"] as? String
                                self.citiSelected = (response["CityId"] as? Int)!
                                let indexCity = self.citiesId.index(of: self.citiSelected)!
                                self.viewOnePicker.selectRow(indexCity, inComponent: 0, animated: true)
                                self.companyCombo.text = response["Company"] as? String
                                self.companySelected = (response["CompanyId"] as? Int)!
                                let indexCompany = self.companiesId.index(of: self.companySelected)!
                                self.viewTwoPicker.selectRow(indexCompany, inComponent: 0, animated: true)
                                let email = response["Email"] as? String
                                let emailArray = email?.characters.split(separator: "@").map(String.init)
                                self.emailServerCombo.text = "@\(((emailArray?[1])! as String))"
                                self.emailText.text = emailArray?[0]
                                self.emailServersSelected = self.emailServersId[self.emailServers.index(of:self.emailServerCombo.text!)!]
                                let indexEmailServer = self.emailServersId.index(of: self.emailServersSelected)!
                                self.viewThreePicker.selectRow(indexEmailServer, inComponent: 0, animated: true)
                                self.fullUserNameLabel.text = response["Name"] as? String
                                
                                self.loadPictureProfile((response["Picture"] as? String)!);
                            }else{
                                self.fullUserNameLabel.text = response["Name"] as? String
                            }
                            self.isDataLoaded = true
                            self.stopAnimating()
                        }else{
                            DispatchQueue.main.sync(execute: {
                                self.stopAnimating()
                            })
                            Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }

    }
    
    func loadPictureProfile(_ imageUrlString : String){
        let imageUrl:URL = URL(string: imageUrlString)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        self.pictureProfile.image = image
    }
    
    func addBtnPicker(){
        
        //Toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //Bar button item
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(pressPickerDoneBtn))
        toolbar.setItems([doneButton], animated: false)
        
        citiesCombo.inputAccessoryView = toolbar
        companyCombo.inputAccessoryView = toolbar
        emailServerCombo.inputAccessoryView = toolbar
        editpictureProfileMask.inputAccessoryView = toolbar
        
    }
    
    func pressPickerDoneBtn(){
        if(pickerShow == 4){
            editPictureViewController = storyBoard.instantiateViewController(withIdentifier: "EditPictureViewController") as! EditPictureViewController
            editPictureViewController.returnImage = UIImage()
            editPictureViewController.fromTakePicture = false
            editPictureViewController.showView = true
            if(pictureOptionChoosed == 0){
                editPictureViewController.action = 0
                self.present(editPictureViewController, animated:false, completion:nil)
            }else if(pictureOptionChoosed == 1){
                editPictureViewController.action = 1
                self.present(editPictureViewController, animated:false, completion:nil)
            }else{
                self.view.endEditing(true)
            }
        }else{
            if(citiSelected == 0 && pickerShow == 1){
                citiesCombo.text = cities[0]
                citiSelected = citiesId[cities.index(of:cities[0])!]
            }
            if(companySelected == 0 && pickerShow == 2){
                companyCombo.text = companies[0]
                companySelected = companiesId[companies.index(of:companies[0])!]
            }
            if(emailServersSelected == 0 && pickerShow == 3){
                emailServerCombo.text = emailServers[0]
                emailServersSelected = emailServersId[emailServers.index(of:emailServers[0])!]
            }
            self.view.endEditing(true)
        }
    }
    
    func loadCities(){
        if(isDataLoaded){
            return
        }
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "CityAPI", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            if let arrayCities = response["arrayCities"] as? [[String: Any]] {
                                for citi in arrayCities{
                                    self.cities.append(String(describing: citi["Name"]!))
                                    self.citiesId.append(citi["Id"]! as! Int)
                                }
                                self.viewOnePicker.selectRow(0, inComponent: 0, animated: true)
                                self.stopAnimating()
                                self.loadCompanies()
                            }
                        }else{
                            DispatchQueue.main.sync(execute: {
                                self.stopAnimating()
                            })
                            Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadCities);
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadCities);
                }
            }
        }
    }
    
    func loadCompanies(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "CompanyAPI", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            if let arrayCities = response["arrayCompanies"] as? [[String: Any]] {
                                for citi in arrayCities{
                                    self.companies.append(String(describing: citi["Name"]!))
                                    self.companiesId.append(citi["Id"]! as! Int)
                                }
                                self.viewTwoPicker.selectRow(0, inComponent: 0, animated: true)
                                self.stopAnimating()
                                self.loadEmailServers()
                            }
                        }else{
                            DispatchQueue.main.sync(execute: {
                                self.stopAnimating()
                            })
                            Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadCompanies);
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadCompanies);
                }
            }
        }
    }
    
    func loadEmailServers(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "DomainAPI/1", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            if let arrayCities = response["arrayDomains"] as? [[String: Any]] {
                                for citi in arrayCities{
                                    self.emailServers.append(String(describing: citi["DomainName"]!))
                                    self.emailServersId.append(citi["Id"]! as! Int)
                                }
                                self.viewThreePicker.selectRow(0, inComponent: 0, animated: true)
                                self.stopAnimating()
                                self.loadData()
                            }
                        }else{
                            DispatchQueue.main.sync(execute: {
                                self.stopAnimating()
                            })
                            Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadEmailServers);
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadEmailServers);
                }
            }
        }
    }

    
    //Delegate methods
    // returns the number of 'columns' to display.
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == viewOnePicker){
            pickerShow = 1
            return cities.count
        }else if(pickerView == viewTwoPicker){
            pickerShow = 2
            return companies.count
        }else if(pickerView == viewThreePicker){
            pickerShow = 3
            return emailServers.count
        }else{
            pickerShow = 4
            return pictureOptions.count
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == viewOnePicker){
            return cities[row]
        }else if(pickerView == viewTwoPicker){
            return companies[row]
        }else if(pickerView == viewThreePicker){
            return emailServers[row]
        }else{
            return pictureOptions[row]
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView == viewOnePicker){
            citiesCombo.text = cities[row]
            citiSelected = citiesId[cities.index(of:cities[row])!]
        }else if(pickerView == viewTwoPicker){
            companyCombo.text = companies[row]
            companySelected = companiesId[companies.index(of:companies[row])!]
        }else if(pickerView == viewThreePicker){
            emailServerCombo.text = emailServers[row]
            emailServersSelected = emailServersId[emailServers.index(of:emailServers[row])!]
        }else{
            pictureOptionChoosed = pictureOptionIds[pictureOptions.index(of:pictureOptions[row])!]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendProfile(_ sender: Any) {
        saveProfile()
    }
    
    func saveProfile(){
        if(moveView){
            moveView = false
            citiesCombo.isEnabled = true
            companyCombo.isEnabled = true
            emailServerCombo.isEnabled = true
            view.endEditing(true)
            self.view.frame.origin.y += keyboardHeight
        }
        
        var isFormFilled = true
        if(citiesCombo.text == ""){
            isFormFilled = false
            changePlaceholderColor(citiesCombo)
        }
        if(companyCombo.text == ""){
            isFormFilled = false
            changePlaceholderColor(companyCombo)
        }
        if(emailText.text == ""){
            isFormFilled = false
            emailText.placeholder = "Ingrese email"
            changePlaceholderColor(emailText)
        }
        if(emailServerCombo.text == ""){
            isFormFilled = false
            changePlaceholderColor(emailServerCombo)
        }
        if(!validateEmail()){
            isFormFilled = false
            Utils().createAlert("Debe ingresar un correo valido.", self)
        }
        
        if(isFormFilled){
            let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
            let userDefaults = UserDefaults.standard
            let userId = userDefaults.integer(forKey: "user_id")
            
            let fullEmail = "\((emailText.text! as String))\((emailServerCombo.text! as String))"
            let parameters = [
                "UserId":userId,
                "CityId":citiSelected,
                "CompanyId":companySelected,
                "Email":fullEmail,
                "Picture":Utils().encodeImageToBase64(pictureProfile.image!)
                ] as [String : Any]
            
            ServerHandler().sendRequest(parameters, "UsersAPI/UpdateUser", "POST") {
                returnData in
                print("Data \(returnData)")
                if let status = returnData["status"] as? Bool {
                    if(status){
                        DispatchQueue.main.sync(execute: {
                            self.stopAnimating()
                            if(!self.fromProfile){
                                let nextViewController = self.storyBoard.instantiateViewController(withIdentifier: "SearchCoworkersViewController") as! SearchCoworkersViewController
                                self.present(nextViewController, animated:true, completion:nil)
                            }else{
                                self.dismiss(animated: true, completion: nil)
                            }
                        });
                    }else{
                        DispatchQueue.main.sync(execute: {
                            self.stopAnimating()
                        })
                        Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.saveProfile);
                    }
                }
            }
        }
    }
    
    func validateEmail()-> Bool {
        let fullEmail = "\(emailText.text!)\(emailServerCombo.text!)"
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: fullEmail)
        return result
    }
    
    func changePlaceholderColor(_ textView : UITextField){
        textView.attributedPlaceholder = NSAttributedString(string: textView.placeholder!, attributes: [NSForegroundColorAttributeName : UIColor.red])
    }
    
    //Allow only up the view when emailText is touched
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        moveView = true
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                if(moveView){
                    moveView = true
                    citiesCombo.isEnabled = false
                    companyCombo.isEnabled = false
                    emailServerCombo.isEnabled = false
                    keyboardHeight = keyboardSize.height
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                if(moveView){
                    moveView = false
                    citiesCombo.isEnabled = true
                    companyCombo.isEnabled = true
                    emailServerCombo.isEnabled = true
                    self.view.frame.origin.y += keyboardSize.height
                }
            }
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailText.resignFirstResponder()
        return true
    }
    
}
