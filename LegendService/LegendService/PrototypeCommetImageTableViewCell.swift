//
//  PrototypeCommetImageTableViewCell.swift
//  LegendService
//
//  Created by informatica on 1/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class PrototypeCommetImageTableViewCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageCont: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        picture.layer.borderWidth = 2
        picture.layer.masksToBounds = false
        picture.layer.borderColor = UIColor.lightGray.cgColor
        picture.layer.cornerRadius = min(picture.frame.height, picture.frame.width)/2.0
        picture.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
