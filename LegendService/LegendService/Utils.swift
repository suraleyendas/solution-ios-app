//
//  Utils.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/16/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation
import UIKit

class Utils{
    
    private
    func getDataFromUrl(urL:NSURL, completion: @escaping ((_ data: NSData?) -> Void)) {
        URLSession.shared.dataTask(with: urL as URL) { (data, response, error) in
            completion(data as NSData?)
            }.resume()
    }
    
    func getCurrentIP(completion:@escaping ((_ ip : NSString?) -> Void)){
        
        if let checkedUrl = NSURL(string: "https://api.ipify.org?format=json") {
            getDataFromUrl(urL: checkedUrl, completion: { (data) -> Void in
                
                let parsedObject: AnyObject? = try! JSONSerialization.jsonObject(with: data! as Data, options: []) as AnyObject?
                
                if let jsonIP = parsedObject as? NSDictionary{
                    
                    DispatchQueue.main.async() {
                        
                        completion(jsonIP["ip"] as? NSString)
                        
                    }
                }
            })
        }
    }
    
    func encodeImageToBase64(_ image: UIImage) -> String{
        let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        let strBase64 = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return strBase64
    }
    
    func decodeImageToBase64(_ strBase64: String) -> UIImage{
        let dataDecoded : Data = Data(base64Encoded: strBase64, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage!
    }
    
    func createAlert(_ message:String, _ view: UIViewController){
        let alert = UIAlertController(title: "Mensaje", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
    
    func createDissmisAlert(_ message:String, _ view: UIViewController){
        let alert = UIAlertController(title: "Mensaje", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in view.dismiss(animated: true, completion: nil)
        }))
        view.present(alert, animated: true, completion: nil)
    }
    
    func createDissmisAlertWithRetry(_ message:String, _ view: UIViewController,_ retryFunc: @escaping ()->()){
        let alert = UIAlertController(title: "Mensaje", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Reintentar", style: UIAlertActionStyle.default, handler:{ (action:UIAlertAction!) in retryFunc()}
        ))
        view.present(alert, animated: true, completion: nil)
    }
    
    func contentInitialSpaces(_ text:String)->Bool{
        if text.characters.index(where: { $0 != " "}) != nil {
            return false
        }
        return true;
    }
    
}

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE =  UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPAD =  UIDevice.current.userInterfaceIdiom == .pad
}

