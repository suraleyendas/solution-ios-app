//
//  PrototypeUserCommentaryTableViewCell.swift
//  LegendService
//
//  Created by informatica on 1/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class PrototypeUserCommentaryTableViewCell: UITableViewCell {

    @IBOutlet weak var userText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
