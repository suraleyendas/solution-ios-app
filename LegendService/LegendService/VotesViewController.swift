//
//  VotesViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/16/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class VotesViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var dataArray = [TableItem]()
    var userId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.layer.borderWidth = 0.50
        tabBar.layer.borderColor = UIColor.clear.cgColor
        tabBar.clipsToBounds = true
        
        //From background
        NotificationCenter.default.addObserver(self, selector:#selector(loadTableData), name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadTableData()
    }
    
    func loadTableData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        //let userDefaults = UserDefaults.standard
        //let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [
            "UserId":userId,
            "SearchQuery":"",
            "Width": 50,
            "Height":50,
            "PageIndex": 0,
            "PageSize": 100
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUserVotersByUserId", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let isEmptyResult = returnData["empty"] as? Bool {
                            if(!isEmptyResult){
                                if let respData = returnData["data"] as? [String : Any] {
                                    if let arrayUserVoters = respData["ArrayUserVoters"] as? [[String: Any]] {
                                        self.dataArray = [TableItem]()
                                            
                                            for userVoter in arrayUserVoters{
                                                self.dataArray.append(TableItem(
                                                    userImage: (userVoter["Picture"] as? String)!,
                                                    userName: (userVoter["Name"] as? String)!,
                                                    userId: (userVoter["UserId"] as? Int)!,
                                                    userEmail: (userVoter["Email"] as? String)!))
                                            }
                                            self.tableView.reloadData()
                                            self.stopAnimating()
                                    }
                                }
                            }else{
                                self.stopAnimating()
                            }
                        }
                    })
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadTableData);
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "voteUserCell", for: indexPath) as! VoteUserTableViewCell
        
        let model = dataArray[indexPath.row]
        cell.voteName.text = model.name
        cell.voteEmail.text = model.email
        cell.voteImage.tag = model.id
        
        if(model.image != ""){
            cell.voteImage.image = Utils().decodeImageToBase64(model.image)
        }else{
            let image : UIImage = UIImage(named:"small_pic_prof.png")!
            cell.voteImage.image = image
        }
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(VotesViewController.goUserProfile(_:)))
        cell.voteImage.addGestureRecognizer(tap)
        
        return cell
    }
    
    func goUserProfile(_ sender: UITapGestureRecognizer){
        if let image = sender.view as? UIImageView {
            let nextViewController = self.storyBoard.instantiateViewController(withIdentifier: "DefaultProfileViewController") as! DefaultProfileViewController
            nextViewController.userProfileId = image.tag
            self.present(nextViewController, animated:true, completion:nil)
            
        }
    }
    
    //add delegate method for pushing to new detail controller
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cellToDeSelect.contentView.backgroundColor = UIColor.white
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    @IBAction func goProfile(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func backVotes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
