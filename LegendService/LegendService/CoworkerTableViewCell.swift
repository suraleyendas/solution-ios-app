//
//  CoworkerTableViewCell.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/20/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class CoworkerTableViewCell: UITableViewCell {

    @IBOutlet weak var coworkerImage: UIImageView!
    @IBOutlet weak var coworkerName: UILabel!
    @IBOutlet weak var coworkerEmail: UILabel!
    @IBOutlet weak var coworkerButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        coworkerImage.layer.borderWidth = 2
        coworkerImage.layer.masksToBounds = false
        coworkerImage.layer.borderColor = UIColor.lightGray.cgColor
        coworkerImage.layer.cornerRadius = min(coworkerImage.frame.height, coworkerImage.frame.width)/2.0
        coworkerImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
