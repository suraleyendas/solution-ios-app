//
//  ShowPublicationViewController.swift
//  LegendService
//
//  Created by informatica on 1/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ShowPublicationViewController: UIViewController, UITextViewDelegate, NVActivityIndicatorViewable {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var textView: UILabel!
    @IBOutlet weak var commentaryContent: UITextView!
    @IBOutlet weak var imageViewConst: NSLayoutConstraint!
    @IBOutlet weak var webViewConst: NSLayoutConstraint!
    @IBOutlet weak var textViewConst: NSLayoutConstraint!
    @IBOutlet weak var commentaryImgConst: NSLayoutConstraint!
    @IBOutlet weak var commentaryVidConst: NSLayoutConstraint!
    @IBOutlet weak var commentaryTxtConst: NSLayoutConstraint!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var pictureProf: UIImageView!
    @IBOutlet weak var shareBtn: UIButton!
    
    var contentType = 0
    var strData = ""
    var picProfileStr = ""
    var userNameStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = userNameStr
        pictureProf.imageFromServerURL(urlString: picProfileStr)
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        commentaryContent.layer.borderColor = myColor.cgColor
        commentaryContent.layer.borderWidth = 1.0
        commentaryContent.layer.cornerRadius = 5.0
        
        pictureProf.layer.borderWidth = 2.0
        pictureProf.layer.masksToBounds = true
        pictureProf.layer.borderColor = UIColor.lightGray.cgColor
        pictureProf.clipsToBounds = true
        
        switch contentType {
            case 1:
                imageView.isHidden = true
                webView.isHidden = true
                imageViewConst.isActive = false
                webViewConst.isActive = false
                commentaryImgConst.isActive = false
                commentaryVidConst.isActive = false
                commentaryContent.isHidden = true
                
                textView.text = strData
                
                break
            case 2:
                imageView.isHidden = true
                textView.isHidden = true
                imageViewConst.isActive = false
                textViewConst.isActive = false
                commentaryImgConst.isActive = false
                commentaryTxtConst.isActive = false
                commentaryVidConst.constant = 10
                
                let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"\(strData)\" ></iframe>"
                webView.loadHTMLString(htmlStr, baseURL: nil)
                webView.scrollView.isScrollEnabled = false
                
                break
            case 3:
                textView.isHidden = true
                webView.isHidden = true
                textViewConst.isActive = false
                webViewConst.isActive = false
                commentaryTxtConst.isActive = false
                commentaryVidConst.isActive = false
                commentaryImgConst.constant = 10
                
                imageView.clipsToBounds = true
                imageView.imageFromServerURL(urlString: strData)
                
                break
            default:
                break
        }
        
        //Hide keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false;
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        pictureProf.layer.cornerRadius = min(pictureProf.bounds.size.height, pictureProf.bounds.size.width)/2.0
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        if(textView.text! == ""){
            shareBtn.isEnabled = true
            shareBtn.setTitleColor(UIColor .black, for: UIControlState.normal)
        }else if(Utils().contentInitialSpaces(textView.text!)){
            shareBtn.isEnabled = false
            shareBtn.setTitleColor(UIColor .gray, for: UIControlState.normal)
        }else{
            shareBtn.isEnabled = true
            shareBtn.setTitleColor(UIColor .black, for: UIControlState.normal)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @IBAction func sharePublication(_ sender: Any) {
        toShare()
    }
    
    func toShare(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        var parameters = [:] as [String : Any]
        var content = commentaryContent.text!
        if(contentType == 1){
            content = strData
            strData = ""
        }
        parameters = [
            "UserId": userId,
            "File":strData,
            "Content":content,
            "IsActive":true,
            "ContentParent":"",
            "ContentTypeId": contentType,
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/SaveContent", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if(dataResp["Result"] as! Bool){
                                self.stopAnimating()
                                self.shareNewPublication(dataResp["ContentId"] as! Int)
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.toShare);
                }
            }
        }
    }
    
    func shareNewPublication(_ newPublicationId:Int){
        
        /*
         UserContentTypeId
         1->Like
         2->Guardar
         3->Reportar
         4->Compartir
         5->Chat
         */
        
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId": userId,
            "ContentId":newPublicationId,
            "UserShareId":"",
            "UserContentTypeId":4
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/SharePublish", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if(dataResp["Result"] as! Bool){
                                self.dismiss(animated: true, completion: nil)
                            }else{
                                Utils().createAlert("Presentamos inconvenientes con el servicio de compartir. Por favor intetelo mas tarde.", self)
                            }
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de compartir. Por favor intetelo mas tarde.", self)
                }
            }
        }
    }
    
    @IBAction func backShowPublication(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
