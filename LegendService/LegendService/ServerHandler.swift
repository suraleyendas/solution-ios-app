//
//  HandlerController.swift
//  LegendService
//
//  Created by informatica on 16/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation
import UIKit

class ServerHandler {
    
    let serverPath = "/BigSocialNetwork/Service/api/"
    
    var headers = [String : String]()
    var apiUN = ""
    var apiPass = ""
    var serverHost = ""
    var fileHeaders = [String : String]()
    
    init() {
        //Create token
        if let apiUserName = Bundle.main.object(forInfoDictionaryKey: "APIUsername"){
            apiUN = apiUserName as! String
        }
        if let apiPassword = Bundle.main.object(forInfoDictionaryKey: "APIPassword"){
            apiPass = apiPassword as! String
        }
        let loginString = String(format: "%@:%@", apiUN, apiPass)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        //Create Headers
        headers = [
            "content-type": "application/json",
            "authorization": "Basic \(base64LoginString)",
            "cache-control": "no-cache"
        ]
        
        fileHeaders = [
            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "authorization": "Basic \(base64LoginString)",
            "cache-control": "no-cache"
        ]
        
        if let serverHst = Bundle.main.object(forInfoDictionaryKey: "ServerHost"){
            serverHost = serverHst as! String
        }
    }
    
    func sendRequest(_ parameters:[String : Any], _ action : String, _ type : String, completionHandler: @escaping (_ returnData: [String : Any]) -> ()){
        
        var jsonResponse = [String : Any]()
        do {
            
            let request = NSMutableURLRequest(url: NSURL(string: "\(serverHost)\(serverPath)\(action)")! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
            request.httpMethod = type
            request.allHTTPHeaderFields = headers
            if(type == "POST"){
                let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
                request.httpBody = postData as Data
            }
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print("Error creando sesion \(error)")
                    jsonResponse["status"] = false
                    jsonResponse["Error"] = "Error creando sesion: \(error)"
                    completionHandler(jsonResponse)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print("Http response \(httpResponse)")
                    if(httpResponse?.statusCode == 200){
                        jsonResponse["status"] = true
                        jsonResponse["empty"] = false
                        let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any]
                        if let vResult = jsonData?["Result"] as? Bool{
                            if(!vResult){jsonResponse["empty"] = true}
                        }
                        if let vresult = jsonData?["result"] as? Bool{
                            if(!vresult){jsonResponse["empty"] = true}
                        }
                        jsonResponse["data"] = jsonData
                        completionHandler(jsonResponse)
                    }else{
                        print("Error al conectar con servidor: \(error)")
                        jsonResponse["status"] = false
                        jsonResponse["Error"] = "Error al conectar con servidor: \(error)"
                        completionHandler(jsonResponse)
                    }
                }
            })
            dataTask.resume()
        } catch  {
            print("Ha ocurrido una exception al ejecutar el metodo POST para \(action)")
            jsonResponse["status"] = false
            jsonResponse["Error"] = "Error creando sesion: \(error)"
            completionHandler(jsonResponse)
        }
    }
    
    func uploadImage(mimetype:String, filePathKey: String, defFileName:String,image:UIImage, quality: CGFloat) -> NSMutableURLRequest{
        
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        let body = NSMutableData()
        
        let parameters = NSMutableDictionary()
        
        for (key, value) in parameters {
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString(string: "\(value)\r\n")
        }
        
        body.appendString(string: "--\(boundary)\r\n")
        
        let imageData = UIImageJPEGRepresentation(image, quality)
        
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(defFileName)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageData!)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        let request = NSMutableURLRequest(url: NSURL(string:"http://test.multistrategy.co/BigSocialNetwork/Service/api/MultimediaAPI/PostUserImage")! as URL)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = fileHeaders
        request.httpBody = body as Data
        
        return request
    }
    
    func uploadVideo(mimetype:String, filePathKey: String, defFileName:String, url:URL, quality: CGFloat) -> NSMutableURLRequest{
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        let body = NSMutableData()
        
        let parameters = NSMutableDictionary()
        
        for (key, value) in parameters {
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString(string: "\(value)\r\n")
        }
        body.appendString(string: "--\(boundary)\r\n")
        
        var videoData = Data()
        if let videoDataW = NSData(contentsOf: url) {
            print(videoDataW.length)
            videoData = videoDataW as Data
        }
        
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(defFileName)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(videoData as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        let request = NSMutableURLRequest(url: NSURL(string:"http://test.multistrategy.co/BigSocialNetwork/Service/api/MultimediaAPI/PostUserImage")! as URL)
        
        //let request = NSMutableURLRequest(url: NSURL(string:"http://anasoft.com.co/upload.php")! as URL)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = fileHeaders
        request.httpBody = body as Data
        
        return request
    }
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
