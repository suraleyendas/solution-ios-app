//
//  MessagesViewController.swift
//  LegendService
//
//  Created by informatica on 13/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var messageArray = [Message]()
    var messageArraySearch = [Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.backgroundImage = UIImage()
        
        messageArray.append(Message.init(id: 1, userId: 4, image: "opponent_icon.png", name: "Andrea Ramirez", textContent: "Hola como estas, te envio este mensaje de prueba. Respondeme los mas pronto posible"))
        messageArray.append(Message.init(id: 2, userId: 6, image: "opponent_icon.png", name: "Juan Pablo Raba", textContent: "Hola Cesar"))
        messageArray.append(Message.init(id: 2, userId: 6, image: "opponent_icon.png", name: "Juan Pablo Raba", textContent: "jfasd fksd fksd fhsf sdfjs fksdf ksfj sdf sdkf sdkfj fksdf ksdf sdhf djfk sdfk sdfhdf sdkf ksdfhsdfh ds"))
        messageArraySearch.append(Message.init(id: 1, userId: 4, image: "opponent_icon.png", name: "Andrea Ramirez", textContent: "Hola como estas, te envio este mensaje de prueba. Respondeme los mas pronto posible"))
        messageArraySearch.append(Message.init(id: 2, userId: 6, image: "opponent_icon.png", name: "Juan Pablo Raba", textContent: "Hola Cesar"))
        messageArraySearch.append(Message.init(id: 2, userId: 6, image: "opponent_icon.png", name: "Juan Pablo Raba", textContent: "jfasd fksd fksd fhsf sdfjs fksdf ksfj sdf sdkf sdkfj fksdf ksdf sdhf djfk sdfk sdfhdf sdkf ksdfhsdfh ds"))
        
    }
    
    // MARK: - search bar delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.endEditing(true)
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.endEditing(true)
        self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            messageArray = messageArraySearch
            self.tableView.reloadData()
        }else {
            filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
        }
    }
    
    func filterTableView(ind:Int,text:String) {
        //fix of not searching when backspacing
        messageArray = messageArraySearch.filter({ (mod) -> Bool in
            
            if(mod.name.lowercased().contains(text.lowercased()) || mod.textContent.lowercased().contains(text.lowercased())){
                return true
            }
            return false
        })
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as! MessagesTableViewCell
        
        let model = messageArray[indexPath.row]
        cell.userImage.image = UIImage(named: model.image)
        cell.userName.text = model.name
        cell.userMessage.text = model.textContent
        
        return cell
    }
    
    //add delegate method for pushing to new detail controller
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cellToDeSelect.contentView.backgroundColor = UIColor.white
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "       ") { (rowAction, indexPath) in
            self.messageArray.remove(at: indexPath.row)
            self.messageArraySearch.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        let imageBck = UIImage(named: "delete_message_icon.png")!
        deleteAction.backgroundColor = UIColor(patternImage: imageBck)
        
        return [deleteAction]
    }

    @IBAction func backMessages(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
