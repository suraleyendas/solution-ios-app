//
//  OptionsPopoverViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/28/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class OptionsPopoverViewController: UIViewController {

    @IBOutlet weak var reportContentBtn: UIButton!
    @IBOutlet weak var saveContentBtn: UIButton!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    var contentId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(contentId)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goReport(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ReportContentViewController") as! ReportContentViewController
        nextViewController.contentId = contentId
        nextViewController.parentCtrl = self
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func saveContent(_ sender: Any) {
        /*
         UserContentTypeId
         1->Like
         2->Guardar
         3->Reportar
         4->Compartir
         5->Chat
         */
        
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId":userId,
            "ContentId":contentId,
            "UserShareId":"",
            "UserContentTypeId":"2",
            "ReportTypeId":"",
            "ReportDescription":"",
            "Viewed":""
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/SaveUserContent", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if(dataResp["Result"] as! Bool){
                                Utils().createDissmisAlert("Contenido guardado correctamente.", self)
                            }
                        }else{
                            Utils().createAlert("Presentamos inconvenientes con el servicio de guardar contenido. Por favor intetelo mas tarde.", self)
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de guardar contenido. Por favor intetelo mas tarde.", self)
                }
            }
        }
    }
}
