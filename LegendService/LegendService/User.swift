//
//  User.swift
//  LegendService
//
//  Created by informatica on 14/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class User: NSObject{
    
    var pictureProfile = ""
    var name = ""
    var email = ""
    
    override init() {}
    
    init(pictureProfile: String, name: String, email: String){
        self.pictureProfile = pictureProfile
        self.name = name
        self.email = email
    }
}
