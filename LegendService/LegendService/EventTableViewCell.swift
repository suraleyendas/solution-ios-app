//
//  EventTableViewCell.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 3/1/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var userPictureProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var eventDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userPictureProfile.layer.borderWidth = 2
        userPictureProfile.layer.masksToBounds = false
        userPictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        userPictureProfile.layer.cornerRadius = min(userPictureProfile.frame.height, userPictureProfile.frame.width)/2.0
        userPictureProfile.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
