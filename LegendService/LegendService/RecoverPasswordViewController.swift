//
//  RecoverPasswordViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/14/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class RecoverPasswordViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var spinnerLoad: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        spinnerLoad.startAnimating()
        let url = NSURL (string: "http://test.multistrategy.co/BigSocialNetwork/Service/Content/Pages/updatePassword.html");
        let requestObj = NSURLRequest(url: url! as URL);
        webView.loadRequest(requestObj as URLRequest);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        spinnerLoad.stopAnimating()
        spinnerLoad.isHidden = true
    }
}
