//
//  PrototypeNineTableViewCell.swift
//  LegendService
//
//  Created by informatica on 27/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

//Text + two commentaries

import UIKit

class PrototypeNineTableViewCell: UITableViewCell {

    @IBOutlet weak var pictureProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var textContent: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var commentaryBtn: UIButton!
    @IBOutlet weak var commentaryCounter: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var firstCommentaryPicture: UIImageView!
    @IBOutlet weak var firstCommentaryUser: UILabel!
    @IBOutlet weak var firstCommentaryText: UILabel!
    @IBOutlet weak var secondCommentaryPicture: UIImageView!
    @IBOutlet weak var secondCommentaryUser: UILabel!
    @IBOutlet weak var secondCommentaryText: UILabel!
    @IBOutlet weak var optionsBar: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pictureProfile.layer.borderWidth = 2
        pictureProfile.layer.masksToBounds = false
        pictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfile.layer.cornerRadius = min(pictureProfile.frame.height, pictureProfile.frame.width)/2.0
        pictureProfile.clipsToBounds = true
        
        firstCommentaryPicture.layer.borderWidth = 2
        firstCommentaryPicture.layer.masksToBounds = false
        firstCommentaryPicture.layer.borderColor = UIColor.lightGray.cgColor
        firstCommentaryPicture.layer.cornerRadius = min(firstCommentaryPicture.frame.height, firstCommentaryPicture.frame.width)/2.0
        firstCommentaryPicture.clipsToBounds = true
        
        secondCommentaryPicture.layer.borderWidth = 2
        secondCommentaryPicture.layer.masksToBounds = false
        secondCommentaryPicture.layer.borderColor = UIColor.lightGray.cgColor
        secondCommentaryPicture.layer.cornerRadius = min(secondCommentaryPicture.frame.height, secondCommentaryPicture.frame.width)/2.0
        secondCommentaryPicture.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
