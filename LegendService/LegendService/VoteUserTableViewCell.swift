//
//  VoteUserTableViewCell.swift
//  LegendService
//
//  Created by informatica on 20/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class VoteUserTableViewCell: UITableViewCell {

    @IBOutlet weak var voteImage: UIImageView!
    @IBOutlet weak var voteName: UILabel!
    @IBOutlet weak var voteEmail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        voteImage.layer.borderWidth = 2
        voteImage.layer.masksToBounds = false
        voteImage.layer.borderColor = UIColor.lightGray.cgColor
        voteImage.layer.cornerRadius = min(voteImage.frame.height, voteImage.frame.width)/2.0
        voteImage.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
