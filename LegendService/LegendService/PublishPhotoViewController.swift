//
//  AddPhotoViewController.swift
//  LegendService
//
//  Created by informatica on 23/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class PublishPhotoViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var commentary: UITextView!
    @IBOutlet weak var imageUploadProgressView: UIProgressView!
    @IBOutlet weak var publicateBtn: UIButton!
    @IBOutlet weak var takePictureBtn: UIButton!
    @IBOutlet weak var choosePictureBtn: UIButton!
    
    let userPicture = UIImage()
    let userName = ""
    var picture = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = .white
        
        publicateBtn.isEnabled = false
        commentary.isHidden = true
        photo.clipsToBounds = true
        imageUploadProgressView.transform = imageUploadProgressView.transform.scaledBy(x: 1, y: 3)
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        commentary.layer.borderColor = myColor.cgColor
        commentary.layer.borderWidth = 1.0
        commentary.layer.cornerRadius = 5
        
        //Up view
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Hide keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false;
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    @IBAction func goPublishPhoto(_ sender: Any) {
        takePictureBtn.isEnabled = false
        publicateBtn.isEnabled = false
        choosePictureBtn.isEnabled = false
        let mimetype = "image/png"
        let filePathKey = "file"
        let now = { round(NSDate().timeIntervalSince1970) }
        let fileName = "ios_\(now).jpg"
        
        let request = ServerHandler().uploadImage(mimetype: mimetype, filePathKey: filePathKey, defFileName: fileName, image: photo.image!, quality: 0.5)
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = session.dataTask(with: request as URLRequest)
        task.resume()
    }
    
    @IBAction func backAddPhoto(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAddPhoto(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func showFullscreen() {
        let imageResize = resizeToScreenSize(image: photo.image!)
        let newImageView = UIImageView(image: imageResize)
        newImageView.frame = self.view.frame
        newImageView.alpha = 0
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        newImageView.fadeIn(withDuration: 0.8)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {
        if(error != nil){
            Utils().createAlert("Error cargando archivo:\(error)", self)
        }
    }
    
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64)
    {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        
        imageUploadProgressView.progress = uploadProgress
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data){
        do {
            if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any] {
                let userDefaults = UserDefaults.standard
                let userId = userDefaults.integer(forKey: "user_id")
                
                let parameters = [
                    "UserId":userId,
                    "File":parsedData["Message"] as! String,
                    "Content":commentary.text!,
                    "IsActive":true,
                    "ContentParent":"",
                    "ContentTypeId":"3",
                    ] as [String : Any]
                
                ServerHandler().sendRequest(parameters, "ContentAPI/SaveContent", "POST") {
                    returnData in
                    print("Data \(returnData)")
                    if let status = returnData["status"] as? Bool {
                        if(status){
                            DispatchQueue.main.sync(execute: {
                                if let dataResp = returnData["data"] as? [String: Any] {
                                    if(dataResp["Result"] as! Bool){
                                        self.dismiss(animated: true, completion: nil)
                                    }else{
                                        Utils().createAlert("Presentamos inconvenientes con el servicio de publicaciones. Por favor intetelo mas tarde.", self)
                                    }
                                }
                            });
                        }else{
                            Utils().createAlert("Presentamos inconvenientes con el servicio de publicaciones. Por favor intetelo mas tarde.", self)
                        }
                    }
                }
            }else{
                Utils().createAlert("Presentamos inconvenientes con el servicio de publicaciones. Por favor intetelo mas tarde.", self)
            }
        }
    }
    
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let tappedImage = sender.view as! UIImageView
        tappedImage.fadeOut(withDuration: 0.8)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        
        image.draw(in: CGRect(x: 0, y: 0,width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func resizeToScreenSize(image: UIImage)->UIImage{
        
        let screenSize = self.view.bounds.size
        return resizeImage(image: image, newWidth: screenSize.width)
    }
    
    @IBAction func takePictureFromCamera(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        self .present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func takePictureFromGallery(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self .present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String : AnyObject]){
        commentary.isHidden = false
        imageUploadProgressView.progress = 0.0
        
        let imageCaptured = info[UIImagePickerControllerOriginalImage] as? UIImage
        let resizeImage = imageCaptured?.resized(toWidth: 800)
        photo.image = resizeImage
        photo.clipsToBounds = true
        let tapShow = UITapGestureRecognizer(target: self, action: #selector(showFullscreen))
        photo.addGestureRecognizer(tapShow)
        picker .dismiss(animated: true, completion: nil)
        publicateBtn.isEnabled = true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

}

public extension UIView {
    func fadeIn(withDuration duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(withDuration duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
}
