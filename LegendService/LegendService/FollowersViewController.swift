//
//  FollowersViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/16/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class FollowersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, UISearchBarDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomBar: UITabBar!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    let userDefaults = UserDefaults.standard
    
    var dataArray = [TableItem]()
    var initialDataAry = [TableItem]()
    var userId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.backgroundImage = UIImage()
        bottomBar.layer.borderWidth = 0.50
        bottomBar.layer.borderColor = UIColor.clear.cgColor
        bottomBar.clipsToBounds = true
        
        //From background
        NotificationCenter.default.addObserver(self, selector:#selector(loadTableData), name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadTableData()
    }
    
    func loadTableData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        //let userDefaults = UserDefaults.standard
        //let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [
            "UserId":userId,
            "SearchQuery":"",
            "Width": 50,
            "Height":50,
            "PageIndex": 0,
            "PageSize": 100
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUserFollowers", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let isEmptyResult = returnData["empty"] as? Bool {
                            if(!isEmptyResult){
                                if let respData = returnData["data"] as? [String : Any] {
                                    if let arrayUserVoters = respData["UserArray"] as? [[String: Any]] {
                                        self.dataArray = [TableItem]()
                                        self.initialDataAry = [TableItem]()
                                        for userVoter in arrayUserVoters{
                                            self.dataArray.append(TableItem(
                                                userImage: (userVoter["Picture"] as? String)!,
                                                userName: (userVoter["Name"] as? String)!,
                                                userId: (userVoter["UserId"] as? Int)!,
                                                userEmail: (userVoter["Email"] as? String)!,
                                                userIsFollow: (userVoter["IsFollow"] as? Bool)!))
                                            self.initialDataAry.append(TableItem(
                                                userImage: (userVoter["Picture"] as? String)!,
                                                userName: (userVoter["Name"] as? String)!,
                                                userId: (userVoter["UserId"] as? Int)!,
                                                userEmail: (userVoter["Email"] as? String)!,
                                                userIsFollow: (userVoter["IsFollow"] as? Bool)!))
                                        }
                                        self.tableView.reloadData()
                                        self.stopAnimating()
                                    }
                                }
                            }else{
                                self.stopAnimating()
                            }
                        }
                    })
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadTableData);
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - search bar delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.endEditing(true)
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.endEditing(true)
        self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            dataArray = initialDataAry
            self.tableView.reloadData()
        }else {
            filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
        }
    }
    
    func filterTableView(ind:Int,text:String) {
        //fix of not searching when backspacing
        dataArray = initialDataAry.filter({ (mod) -> Bool in
            if(mod.name.lowercased().contains(text.lowercased()) || mod.email.lowercased().contains(text.lowercased())){
                return true
            }
            return false
        })
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "followerUserCell", for: indexPath) as! FollowerUserTableViewCell
        
        let model = dataArray[indexPath.row]
        cell.followerName.text = model.name
        cell.followerEmail.text = model.email
        cell.followerImage.tag = model.id
        cell.followerBtn.tag = model.id
        cell.followerBtn.accessibilityIdentifier = String(model.isFollow)
        cell.followerImage.accessibilityIdentifier = String(model.isFollow)
        
        if(model.image != ""){
            cell.followerImage.image = Utils().decodeImageToBase64(model.image)
        }else{
            let image : UIImage = UIImage(named:"small_pic_prof.png")!
            cell.followerImage.image = image
        }
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(FollowersViewController.goUserProfile(_:)))
        cell.followerImage.isUserInteractionEnabled = true
        cell.followerImage.addGestureRecognizer(tap)
        
        //Add event button
        let tapBtn = UITapGestureRecognizer(target: self, action:#selector(FollowersViewController.followActionBtn(_:)))
        cell.followerBtn.addGestureRecognizer(tapBtn)
        
        //Add Btn image by follow state
        if(!model.isFollow){
            cell.followerBtn.setImage(UIImage(named: "follow_user_off.png") as UIImage?, for: .normal)
        }else{
            cell.followerBtn.setImage(UIImage(named: "follow_user_on.png") as UIImage?, for: .normal)
        }

        return cell
    }
    
    func goUserProfile(_ sender: UITapGestureRecognizer){
        if let image = sender.view as? UIImageView {
            let nextViewController = self.storyBoard.instantiateViewController(withIdentifier: "DefaultProfileViewController") as! DefaultProfileViewController
            nextViewController.userProfileId = image.tag
            let accessIdenty:String = image.accessibilityIdentifier!
            var ifollowUser = false
            if(accessIdenty == "true"){
                ifollowUser = true
            }
            nextViewController.userProfileIsFollow = ifollowUser
            self.present(nextViewController, animated:true, completion:nil)
            
        }
    }
    
    func followActionBtn(_ sender: UITapGestureRecognizer){
        if let button = sender.view as? UIButton {
            var followUser = false
            let accessIdenty:String = button.accessibilityIdentifier!
            if(accessIdenty == "false"){
                followUser = true
            }
            let userId = self.userDefaults.integer(forKey: "user_id")
            let followUserId : Int = button.tag
            
            let parameters = [
                "UserId":userId ,
                "UserFollowId": followUserId,
                "IsFollow": followUser
                ] as [String : Any]
            
            let hand = ServerHandler().sendRequest(parameters, "UsersAPI/SaveUserFollow", "POST") {
                returnData in
                print("Data \(returnData)")
                if let status = returnData["status"] as? Bool {
                    if(status){
                        DispatchQueue.main.sync(execute: {
                            if(!followUser){
                                button.setImage(UIImage(named: "follow_user_off.png") as UIImage?, for: .normal)
                            }else{
                                button.setImage(UIImage(named: "follow_user_on.png") as UIImage?, for: .normal)
                            }
                            if let itemTable = self.dataArray.filter({$0.id == followUserId}).first
                            {
                                itemTable.isFollow = followUser
                            }
                        });
                    }else{
                        Utils().createAlert("Presentamos inconvenientes con el servicio de seguir compañeros. Por favor intetelo mas tarde.", self)
                    }
                }
            }
            
        }
    }
    
    //add delegate method for pushing to new detail controller
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cellToDeSelect.contentView.backgroundColor = UIColor.white
    }
    @IBAction func goProfile(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backFollower(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
