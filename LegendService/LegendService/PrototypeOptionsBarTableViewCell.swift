//
//  OptionsBarTableViewCell.swift
//  LegendService
//
//  Created by informatica on 8/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class PrototypeOptionsBarTableViewCell: UITableViewCell {

    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentCounter: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
