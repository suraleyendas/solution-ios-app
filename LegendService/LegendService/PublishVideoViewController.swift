//
//  PublishVideoViewController.swift
//  LegendService
//
//  Created by informatica on 23/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation

class PublishVideoViewController: UIViewController, URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {

    @IBOutlet weak var videoContent: UIWebView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var commentary: UITextView!
    @IBOutlet weak var videoUploadProgressView: UIProgressView!
    @IBOutlet weak var publicateBtn: UIButton!
    @IBOutlet weak var takeVideoBtn: UIButton!
    @IBOutlet weak var chooseVideoBtn: UIButton!
    
    let imagePicker: UIImagePickerController! = UIImagePickerController()
    var urlVideo = URL(string: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        publicateBtn.isEnabled = false
        UIApplication.shared.statusBarView?.backgroundColor = .white
        videoUploadProgressView.transform = videoUploadProgressView.transform.scaledBy(x: 1, y: 3)
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        commentary.layer.borderColor = myColor.cgColor
        commentary.layer.borderWidth = 1.0
        commentary.layer.cornerRadius = 5
        
        
        let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><image width=\"100%\"; style=\"margin-top:20%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"http://www.clipartbest.com/cliparts/9i4/6pa/9i46paR4T.png\" ></image>"
        videoContent.loadHTMLString(htmlStr, baseURL: nil)
        
        //Up view
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Hide keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false;
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func captureVideo(_ sender: Any) {
        
        imagePicker.sourceType = .camera
        imagePicker.mediaTypes = [kUTTypeMovie as String]
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: {})
    }
    
    @IBAction func loadVideo(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeMovie as String]
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: {})
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedVideo:URL = (info[UIImagePickerControllerMediaURL] as? URL) {
            // Save video to the main photo album
            videoContent.loadRequest( NSURLRequest(url: pickedVideo) as URLRequest )
            urlVideo = pickedVideo
            videoUploadProgressView.progress = 0.0
            publicateBtn.isEnabled = true
        }
        
        imagePicker.dismiss(animated: true, completion: {
            // Anything you want to happen when the user saves an video
        })
    }
    
    // Called when the user selects cancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: {
            // Anything you want to happen when the user selects cancel
        })
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64)
    {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        
        videoUploadProgressView.progress = uploadProgress
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data){
        do {
            if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any] {
                print(parsedData)
                let userDefaults = UserDefaults.standard
                let userId = userDefaults.integer(forKey: "user_id")
                
                let parameters = [
                    "UserId": userId,
                    "File":parsedData["Message"] as! String,
                    "Content":commentary.text,
                    "IsActive":true,
                    "ContentParent":"",
                    "ContentTypeId":"2",
                    ] as [String : Any]
                
                ServerHandler().sendRequest(parameters, "ContentAPI/SaveContent", "POST") {
                    returnData in
                    print("Data \(returnData)")
                    if let status = returnData["status"] as? Bool {
                        if(status){
                            DispatchQueue.main.sync(execute: {
                                if let dataResp = returnData["data"] as? [String: Any] {
                                    if(dataResp["Result"] as! Bool){
                                        self.dismiss(animated: true, completion: nil)
                                    }else{
                                        Utils().createAlert("Presentamos inconvenientes con el servicio de publicaciones. Por favor intetelo mas tarde.", self)
                                    }
                                }
                            });
                        }else{
                            Utils().createAlert("Presentamos inconvenientes con el servicio de publicaciones. Por favor intetelo mas tarde.", self)
                        }
                    }
                }
            }else{
                Utils().createAlert("Presentamos inconvenientes con el servicio de publicaciones. Por favor intetelo mas tarde.", self)
            }

        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func goPublishVideo(_ sender: Any) {
        publicateBtn.isEnabled = false
        takeVideoBtn.isEnabled = false
        chooseVideoBtn.isEnabled = false
        let mimetype = "video/mp4"
        let filePathKey = "file"
        let now = { round(NSDate().timeIntervalSince1970) }
        let fileName = "ios_\(now).mp4"
        
        let request = ServerHandler().uploadVideo(mimetype: mimetype, filePathKey: filePathKey, defFileName: fileName, url: urlVideo!, quality: 0.5)
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = session.dataTask(with: request as URLRequest)
        task.resume()
    }

    @IBAction func backPublishVideo(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
