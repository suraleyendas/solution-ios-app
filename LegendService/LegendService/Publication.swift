//
//  Publication.swift
//  LegendService
//
//  Created by informatica on 22/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class Publication: NSObject {
    
    var id = 0
    var userId = 0
    var filePath = ""
    var userCommentary = ""
    var isActive = true
    var contentTypeId:Int = 0
    var contentType:String = ""
    var UserContentTypeId = 0
    var UserContentType = ""
    var isLike = true
    var countLike = 0
    var countComment = 0
    var picture:String = ""
    var name:String = ""
    var commetaries = [Commentary]()
    
    override init() {
        
    }
    
    init(id:Int, userId:Int, filePath:String, userCommentary:String, isActive:Bool, contentTypeId:Int, contentType:String, UserContentTypeId:Int, UserContentType:String, isLike:Bool, countLike:Int, countComment:Int, picture:String, name:String, commetaries:[Commentary]){
        
        self.id = id
        self.userId = userId
        self.filePath = filePath
        self.userCommentary = userCommentary
        self.isActive = isActive
        self.contentTypeId = contentTypeId
        self.contentType = contentType
        self.UserContentTypeId = UserContentTypeId
        self.UserContentType = UserContentType
        self.isLike = isLike
        self.countLike = countLike
        self.countComment = countComment
        self.picture = picture
        self.name = name
        self.commetaries = commetaries
    }
}
