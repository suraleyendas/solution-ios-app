//
//  Report.swift
//  LegendService
//
//  Created by informatica on 28/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class Report: NSObject {
    
    var id = 0
    var text = ""
    
    init(id:Int, text: String){
        self.id = id
        self.text = text
    }
}
