//
//  LegendViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 3/4/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class LegendViewController: UIViewController, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UITextFieldDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var legendImage: UIImageView!
    @IBOutlet weak var legendName: UILabel!
    @IBOutlet weak var legnedDescription: UILabel!
    @IBOutlet weak var legendPeriod: UILabel!
    @IBOutlet weak var legendEmail: UILabel!
    @IBOutlet weak var legendCompanyCity: UILabel!
    @IBOutlet weak var legendFollowBtn: UIButton!
    @IBOutlet weak var pageCount: UIPageControl!
    @IBOutlet weak var swipViewGesture: UIView!
    @IBOutlet weak var chartView: UIView!
    @IBOutlet weak var searchContent: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var selectCompanyContent: UIView!
    @IBOutlet weak var selectCompany: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textHeight: NSLayoutConstraint!
    @IBOutlet weak var spaceUpText: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var hideShowBtn: UIButton!
    
    var count = 0
    var isFinish = false
    var chartRect = CGRect()
    var userArray = [TableItem]()
    var searchArray = [TableItem]()
    var legendArray = [Legend]()
    var defaultLabelHeight: CGFloat?
    var defaultLabelSpacing: CGFloat?
    var scrollSize = CGSize()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(UIScreen.main.bounds.width > 375){
            self.textHeight.constant = self.textHeight.constant - (self.textHeight.constant * 0.17)
        }
        defaultLabelHeight = self.textHeight.constant
        defaultLabelSpacing = self.spaceUpText.constant
        
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        userArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Julio Cesar",
            userId: 2,
            userEmail: "julio@gmail.com",
            isVote: true,
            voteNumber: 23))
        searchArray.append(TableItem.init(
            userImage: "",
            userName: "Adrian Diaz",
            userId: 2,
            userEmail: "adrian@gmail.com",
            isVote: false,
            voteNumber: 23))
        
        //Set selected awards item
        pageCount.currentPage = 0
        
        //Border content
        searchContent.layer.borderColor = UIColor.lightGray.cgColor
        searchContent.layer.borderWidth = 1.0;
        searchContent.layer.cornerRadius = 5.0;
        selectCompanyContent.layer.borderColor = UIColor.lightGray.cgColor
        selectCompanyContent.layer.borderWidth = 1.0;
        selectCompanyContent.layer.cornerRadius = 5.0;
        
        //Banner
        legendImage.clipsToBounds = true
        
        let gestureRight = UISwipeGestureRecognizer(target: self, action: #selector(right))
        gestureRight.direction = .right
        gestureRight.delegate = self
        swipViewGesture.addGestureRecognizer(gestureRight)
        
        let gestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(left))
        gestureLeft.direction = .left
        gestureLeft.delegate = self
        swipViewGesture.addGestureRecognizer(gestureLeft)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(UIScreen.main.bounds.width < 375){
            self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.width, height: self.scrollView.contentSize.height - (self.scrollView.contentSize.height * 0.1))
        }
        loadBannerData()
    }
    
    func loadBannerData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        ServerHandler().sendRequest(parameters, "ContentAPI/GetLegendsOfTheService/\(userId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any]
                        {
                            if let reportArray = dataResp["ArrayLegends"] as? [[String: Any]]{
                                for reportObj in reportArray{
                                    self.legendArray.append(
                                        Legend.init(
                                            trimestre: reportObj["Trimester"] as! Int,
                                            year: reportObj["Year"] as! Int,
                                            userId: reportObj["UserId"] as! Int,
                                            name: reportObj["Name"] as! String,
                                            picture: reportObj["Picture"] as! String,
                                            email: reportObj["Mail"] as! String,
                                            company: reportObj["Company"] as! String,
                                            city: reportObj["City"] as! String,
                                            isFollow: reportObj["IsFollow"] as! Bool)
                                    )
                                }
                                self.pageCount.numberOfPages = self.legendArray.count
                                let firstLegend = self.legendArray[0]
                                self.legendName.text = firstLegend.name
                                self.legendImage.image = Utils().decodeImageToBase64(firstLegend.picture)
                                self.legendPeriod.text = "trimestre \(firstLegend.trimestre)/\(firstLegend.year)"
                                self.legendEmail.text = firstLegend.email
                                self.legendCompanyCity.text = "\(firstLegend.company) - \(firstLegend.city)"
                                self.loadChart()
                                self.stopAnimating()
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadBannerData);
                }
            }
        }
    }
    
    func loadChart(){
        //Chart
        let barChart = PNBarChart(frame: chartView.bounds)
        barChart.backgroundColor = UIColor.clear
        barChart.animationType = .Waterfall
        barChart.labelMarginTop = 5.0
        barChart.xLabels = ["Sura \n Colombia", "ARL \n Sura", "EPS \n Sura", "IPS \n Sura", "Dinámica", "Gestión \n del riesgo", "Otros"]
        barChart.yValues = [1, 23, 12, 18, 30, 12, 21]
        barChart.colors = [
            UIColor.init(red: 0/255, green: 51/255, blue: 160/255, alpha: 1),
            UIColor.init(red: 118/255, green: 189/255, blue: 67/255, alpha: 1),
            UIColor.init(red: 2/255, green: 160/255, blue: 221/255, alpha: 1),
            UIColor.init(red: 6/255, green: 175/255, blue: 169/255, alpha: 1),
            UIColor.init(red: 0/255, green: 119/255, blue: 101/255, alpha: 1),
            UIColor.init(red: 0/255, green: 99/255, blue: 167/255, alpha: 1),
            UIColor.init(red: 241/255, green: 139/255, blue: 33/255, alpha: 1)
        ]
        barChart.strokeChart()
        
        let dataArr = [60.1, 160.1, 126.4, 232.2, 186.2, 127.2, 176.2]
        let data = PNLineChartData()
        data.color = PNGreen
        data.itemCount = dataArr.count
        data.inflexPointStyle = .None
        data.getData = ({
            (index: Int) -> PNLineChartDataItem in
            let yValue = CGFloat(dataArr[index])
            let item = PNLineChartDataItem(y: yValue)
            return item
        })
        
        // Change the chart you want to present here
        chartView.addSubview(barChart)
    }
    
    // MARK: - search bar delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.endEditing(true)
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            userArray = searchArray
            self.tableView.reloadData()
        }else {
            filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
        }
    }
    
    func filterTableView(ind:Int,text:String) {
        //fix of not searching when backspacing
        userArray = searchArray.filter({ (mod) -> Bool in
            if(mod.name.lowercased().contains(text.lowercased()) || mod.email.lowercased().contains(text.lowercased())){
                return true
            }
            return false
        })
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return userArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "voteCell", for: indexPath) as! VoteUITableViewCell
        
        let model = userArray[indexPath.row]
        
        if(model.image != ""){
            cell.voteImage.image = Utils().decodeImageToBase64(model.image)
        }else{
            let image : UIImage = UIImage(named:"small_pic_prof.png")!
            cell.voteImage.image = image
        }
        cell.voteName.text = model.name
        cell.voteEmail.text = model.email
        cell.voteCount.text = "\(model.voteNumber) votos"
        if(model.isVote){
            cell.voteBtn.setImage(UIImage(named: "vote_fill_btn.png"), for: .normal)
        }else{
            cell.voteBtn.setImage(UIImage(named: "vote_empty_btn.png"), for: .normal)
        }
        
        return cell
    }

    func right(gesture: UIGestureRecognizer){
        if(count > 0){
            let legend = self.legendArray[count - 1]
            self.legendName.text = legend.name
            if(legend.picture != ""){
                self.legendImage.image = Utils().decodeImageToBase64(legend.picture)
            }
            self.legendPeriod.text = "trimestre \(legend.trimestre)/\(legend.year)"
            self.legendEmail.text = legend.email
            self.legendCompanyCity.text = "\(legend.company) - \(legend.city)"
            
            self.legendName.rightToLeftAnimation()
            self.legendImage.rightToLeftAnimation()
            self.legendPeriod.rightToLeftAnimation()
            self.legendEmail.rightToLeftAnimation()
            self.legendCompanyCity.rightToLeftAnimation()
            
            count = count - 1
            pageCount.currentPage = count
        }
    }
    
    func left(gesture: UIGestureRecognizer){
        if(count < (legendArray.count - 1)){
            let legend = self.legendArray[count + 1]
            self.legendName.text = legend.name
            if(legend.picture != ""){
                self.legendImage.image = Utils().decodeImageToBase64(legend.picture)
            }
            self.legendPeriod.text = "trimestre \(legend.trimestre)/\(legend.year)"
            self.legendEmail.text = legend.email
            self.legendCompanyCity.text = "\(legend.company) - \(legend.city)"
            
            self.legendName.leftToRightAnimation()
            self.legendImage.leftToRightAnimation()
            self.legendPeriod.leftToRightAnimation()
            self.legendEmail.leftToRightAnimation()
            self.legendCompanyCity.leftToRightAnimation()
            
            count = count + 1
            pageCount.currentPage = count
        }
    }
    
    @IBAction func showHideText(_ sender: Any) {
        if(self.textHeight.constant == 0){
            hideShowBtn.setImage(UIImage(named:"show_icon.png"), for: .normal)
        }else{
            hideShowBtn.setImage(UIImage(named:"hide_icon.png"), for: .normal)
        }
        
        self.textHeight.constant = self.textHeight.constant == 0 ? defaultLabelHeight! : 0
        self.spaceUpText.constant = self.spaceUpText.constant == 0 ? defaultLabelSpacing! : 0
        self.view.setNeedsLayout()
        scrollSize = self.scrollView.contentSize
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.width, height: self.spaceUpText.constant == 0 ? scrollSize.height - self.defaultLabelHeight!: scrollSize.height + self.defaultLabelHeight!)
        
        // Bonus: animating change
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func goProfile(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
