//
//  OptionsViewController.swift
//  LegendService
//
//  Created by informatica on 10/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var optionsArrayNames = [String]()
    var optionsArrayIcons = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        optionsArrayNames = ["Contenidos guardados", "Contenidos que me gustan", "Términos y condiciones", "Preguntas frecuentes", "Cerrar sesión", "Perfil de usuario"]
        optionsArrayIcons = ["publications_saved_icon.png", "publications_like_me_icon.png", "conditions_icon.png", "fecuent_questions_icon.png", "sign_out_icon.png", "user_empty_top_bar_icon.png"]
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return optionsArrayNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionsTableViewCell
        
        let optionName = self.optionsArrayNames[indexPath.row]
        let optionIcon = self.optionsArrayIcons[indexPath.row]
        cell.optionName.text = optionName
        cell.optionIcon.image = UIImage(named: optionIcon)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
            nextViewController.type = "content_saved"
            self.present(nextViewController, animated: true, completion: nil)
            break
        case 1:
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
            nextViewController.type = "content_like"
            self.present(nextViewController, animated: true, completion: nil)
            break
        case 2:
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
            self.present(nextViewController, animated: true, completion: nil)
            break
        case 3:
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FrequentQuestionsViewController") as! FrequentQuestionsViewController
            self.present(nextViewController, animated: true, completion: nil)
            break
        case 4:
            //Sign Out
            break
        case 5:
            //User Profile
            break
        default:
            break
        }
    }
    
    @IBAction func backOptions(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
}
