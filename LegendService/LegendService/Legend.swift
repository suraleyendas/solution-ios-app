//
//  Legend.swift
//  LegendService
//
//  Created by informatica on 6/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class Legend: NSObject{
    
    var trimestre = 0
    var year = 2017
    var userId = 2
    var name = ""
    var picture = ""
    var email = ""
    var company = ""
    var city = ""
    var isFollow = false
    
    init(trimestre: Int, year: Int, userId: Int, name: String, picture: String, email: String, company: String, city: String, isFollow: Bool) {
        
        self.trimestre = trimestre
        self.year = year
        self.userId = userId
        self.name = name
        self.picture = picture
        self.email = email
        self.company = company
        self.city = city
        self.isFollow = isFollow
    }
    
}
