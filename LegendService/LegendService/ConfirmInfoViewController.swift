//
//  ConfirmInfoViewController.swift
//  LegendService
//
//  Created by informatica on 13/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ConfirmInfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
