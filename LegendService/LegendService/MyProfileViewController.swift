//
//  DefaultProfileViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/19/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController, UITabBarDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var pictProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userCimpanyCity: UILabel!
    @IBOutlet weak var userSummary: UILabel!
    @IBOutlet weak var pointsNumber: UILabel!
    @IBOutlet weak var followNumber: UILabel!
    @IBOutlet weak var followerNumber: UILabel!
    @IBOutlet weak var votesNumber: UILabel!
    @IBOutlet weak var followName: UILabel!
    @IBOutlet weak var followerName: UILabel!
    @IBOutlet weak var voteName: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    var isFromMain = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(!isFromMain){
            let userDefaults = UserDefaults.standard
            userDefaults.set(5, forKey: "root_view")
        }
        
        //Add gesture to labels
        let tapFollow = UITapGestureRecognizer(target: self, action: #selector(goFollowViewController))
        followNumber.isUserInteractionEnabled = true
        followNumber.addGestureRecognizer(tapFollow)
        let tapFollowName = UITapGestureRecognizer(target: self, action: #selector(goFollowViewController))
        followName.isUserInteractionEnabled = true
        followName.addGestureRecognizer(tapFollowName)
        let tapFollower = UITapGestureRecognizer(target: self, action: #selector(goFollowerViewController))
        followerNumber.isUserInteractionEnabled = true
        followerNumber.addGestureRecognizer(tapFollower)
        let tapFollowerName = UITapGestureRecognizer(target: self, action: #selector(goFollowerViewController))
        followerNumber.isUserInteractionEnabled = true
        followerName.isUserInteractionEnabled = true
        followerName.addGestureRecognizer(tapFollowerName)
        let tapVotes = UITapGestureRecognizer(target: self, action: #selector(goVotesViewController))
        votesNumber.isUserInteractionEnabled = true
        votesNumber.addGestureRecognizer(tapVotes)
        let tapVotesName = UITapGestureRecognizer(target: self, action: #selector(goVotesViewController))
        voteName.isUserInteractionEnabled = true
        voteName.addGestureRecognizer(tapVotesName)
        let tapSummary = UITapGestureRecognizer(target: self, action: #selector(editSummary))
        userSummary.isUserInteractionEnabled = true
        userSummary.addGestureRecognizer(tapSummary)
        
    }
    
    func editSummary(){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateSummaryViewController") as! UpdateSummaryViewController
        nextViewController.image = pictProfile.image!
        nextViewController.name = userName.text!
        nextViewController.currentSummary = userSummary.text!
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadData()
        scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func loadData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUser/\(userId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            self.loadPictureProfile((response["Picture"] as? String)!);
                            self.userName.text = (response["Name"] as? String)!
                            self.userEmail.text = (response["Email"] as? String)!
                            self.userCimpanyCity.text = "\((response["Company"] as? String)!) - \((response["City"] as? String)!)"
                            self.userSummary.text = (response["Summary"] as? String)!
                        }else{
                            Utils().createDissmisAlert("Presentamos inconvenientes con el servicio al recuperar usuario. Por favor intetelo mas tarde.", self)
                        }
                        self.stopAnimating()
                        self.loadLegendValues()
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }
    }
    
    func loadLegendValues(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetProfileInfoByUserId/\(userId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                             self.pointsNumber.text = "\((response["Points"] as? Int)!)"
                             self.followNumber.text = "\((response["Follows"] as? Int)!)"
                             self.followerNumber.text = "\((response["Followers"] as? Int)!)"
                             self.votesNumber.text = "\((response["Votes"] as? Int)!)"
                        }else{
                            self.pointsNumber.text = "0"
                            self.followNumber.text = "0"
                            self.followerNumber.text = "0"
                            self.votesNumber.text = "0"
                        }
                        self.stopAnimating()
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }
        
    }
    
    func loadPictureProfile(_ imageUrlString : String){
        let imageUrl:URL = URL(string: imageUrlString)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        self.pictProfile.image = image
    }
    
    func goFollowViewController(){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FollowViewController") as! FollowViewController
        let userDefaults = UserDefaults.standard
        nextViewController.userId = userDefaults.integer(forKey: "user_id")
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func goFollowerViewController(){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FollowersViewController") as! FollowersViewController
        let userDefaults = UserDefaults.standard
        nextViewController.userId = userDefaults.integer(forKey: "user_id")
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func goVotesViewController(){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "VotesViewController") as! VotesViewController
        let userDefaults = UserDefaults.standard
        nextViewController.userId = userDefaults.integer(forKey: "user_id")
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func goProfile(_ sender: Any) {
        let nextViewController = self.storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        nextViewController.fromProfile = true
        self.present(nextViewController, animated:true, completion:nil)

    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExploreTabViewController") as! ExploreTabViewController
        nextViewController.itemChoosed = item.tag - 1
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func showOptions(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition)

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "OptionsViewController") as! OptionsViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
}
