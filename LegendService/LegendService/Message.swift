//
//  Message.swift
//  LegendService
//
//  Created by informatica on 13/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class Message:NSObject {
    
    var image = ""
    var name = ""
    var textContent = ""
    var id = 0
    var userId = 0
    
    init(id: Int, userId: Int, image: String, name: String, textContent: String){
        self.id = id
        self.userId = userId
        self.image = image
        self.name = name
        self.textContent = textContent
    }
    
}
