//
//  ReportContentViewController.swift
//  LegendService
//
//  Created by informatica on 28/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ReportContentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var otherOptionText: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reportContentBtn: UIButton!
    
    var dataArray = [Report]()
    var contentId = 0
    var optionChoosedId = 0
    var otherIsChoosed = false
    var parentCtrl = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        otherOptionText.layer.borderColor = myColor.cgColor
        otherOptionText.layer.borderWidth = 1.0
        otherOptionText.layer.cornerRadius = 5
        otherOptionText.isHidden = true
        
        reportContentBtn.isEnabled = false
        
        //Up view
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Hide keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false;
        view.addGestureRecognizer(tap)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadReportOptions()
    }
    
    func loadReportOptions(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        
        let hand = ServerHandler().sendRequest(parameters, "ContentReportTypeAPI", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if let reportArray = dataResp["arrayContentReportType"] as? [[String: Any]]{
                                for reportObj in reportArray{
                                    self.dataArray.append(
                                        Report.init(
                                            id: (reportObj["Id"] as? Int)!,
                                            text: (reportObj["Description"] as? String)!
                                        )
                                    )
                                }
                                self.tableView.reloadData()
                                self.stopAnimating()
                                self.tableView.flashScrollIndicators()
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadReportOptions);
                }
            }
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reportCell", for: indexPath)
        let report = self.dataArray[indexPath.row]
        cell.textLabel?.text = report.text
        cell.textLabel?.tag = report.id
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.numberOfLines = 2
        cell.contentScaleFactor = 0.5
        
        if(cell.isSelected){
            cell.contentView.backgroundColor = UIColor.init(red: 37/255.0, green: 100/255.0, blue: 138/255.0, alpha: 1)
            cell.textLabel?.textColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemSelected = dataArray[indexPath.row]
        if(itemSelected == dataArray[dataArray.count - 1]){
            otherOptionText.isHidden = false
            otherIsChoosed = true
            reportContentBtn.isEnabled = false
        }else{
            otherOptionText.isHidden = true
            otherIsChoosed = false
        }
        optionChoosedId = itemSelected.id
        reportContentBtn.isEnabled = true
        
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.init(red: 37/255.0, green: 100/255.0, blue: 138/255.0, alpha: 1)
        cell?.textLabel?.textColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.textLabel?.textColor = UIColor.black
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        if(Utils().contentInitialSpaces(textView.text!)){
            reportContentBtn.isEnabled = false
        }
        else{
            reportContentBtn.isEnabled = true
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func reportContent(_ sender: Any) {
        toReport()
    }
    
    func toReport(){
        if(otherOptionText.text == "" && otherIsChoosed){
            Utils().createAlert("Ingrese el motivo", self)
        }else{
            /*
             UserContentTypeId
             1->Like
             2->Guardar
             3->Reportar
             4->Compartir
             5->Chat
             */
            let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
            let userDefaults = UserDefaults.standard
            let userId = userDefaults.integer(forKey: "user_id")
            
            let parameters = [
                "UserId": userId,
                "ContentId":contentId,
                "UserShareId":"",
                "UserContentTypeId":"3",
                "ReportTypeId":optionChoosedId,
                "ReportDescription":otherOptionText.text!,
                "Viewed":""
                ] as [String : Any]
            
            ServerHandler().sendRequest(parameters, "ContentAPI/SaveUserContent", "POST") {
                returnData in
                print("Data \(returnData)")
                if let status = returnData["status"] as? Bool {
                    if(status){
                        DispatchQueue.main.sync(execute: {
                            if let dataResp = returnData["data"] as? [String: Any] {
                                if(dataResp["Result"] as! Bool){
                                    self.stopAnimating()
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }
                        });
                    }else{
                        DispatchQueue.main.sync(execute: {
                            self.stopAnimating()
                        })
                        Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.toReport);
                    }
                }
            }
        }
    }
    
    @IBAction func backReportContent(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.parentCtrl.dismiss(animated: true, completion: nil)
        })
    }

}
