//
//  Award.swift
//  LegendService
//
//  Created by informatica on 13/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class Award: NSObject{
    
    var id = 0
    var image = ""
    var title = ""
    var subtitle = ""
    var points = 0
    
    
    init(id: Int, image: String, title: String, subtitle: String, points: Int) {
        self.id = id
        self.image = image
        self.title = title
        self.subtitle = subtitle
        self.points = points
    }
    
}
