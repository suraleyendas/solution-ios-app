//
//  userVote.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 2/19/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import Foundation

class TableItem: NSObject {
    var image:String = ""
    var name:String = ""
    var email:String = ""
    var id:Int = 0
    var isFollow:Bool = false
    var isVote = false
    var voteNumber = 0
    
    init(userImage:String,userName:String, userId:Int, userEmail:String) {
        self.image = userImage
        self.name = userName
        self.id = userId
        self.email = userEmail
    }
    
    init(userImage:String,userName:String, userId:Int, userEmail:String, userIsFollow:Bool) {
        self.image = userImage
        self.name = userName
        self.id = userId
        self.email = userEmail
        self.isFollow = userIsFollow
    }
    
    init(userImage:String,userName:String, userId:Int, userEmail:String, isVote:Bool, voteNumber: Int) {
        self.image = userImage
        self.name = userName
        self.id = userId
        self.email = userEmail
        self.isVote = isVote
        self.voteNumber = voteNumber
    }
}
