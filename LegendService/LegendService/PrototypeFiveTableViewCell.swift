//
//  PrototypeFiveTableViewCell.swift
//  LegendService
//
//  Created by informatica on 27/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

//Video + one commentaries

import UIKit

class PrototypeFiveTableViewCell: UITableViewCell {

    @IBOutlet weak var pictureProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var videoContent: UIWebView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var commentaryBtn: UIButton!
    @IBOutlet weak var commentaryCounter: UILabel!
    @IBOutlet weak var userCommentary: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var commentaryPicture: UIImageView!
    @IBOutlet weak var commentaryName: UILabel!
    @IBOutlet weak var commentaryText: UILabel!
    @IBOutlet weak var optionsBar: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pictureProfile.layer.borderWidth = 2
        pictureProfile.layer.masksToBounds = false
        pictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfile.layer.cornerRadius = min(pictureProfile.frame.height, pictureProfile.frame.width)/2.0
        pictureProfile.clipsToBounds = true
        
        commentaryPicture.layer.borderWidth = 2
        commentaryPicture.layer.masksToBounds = false
        commentaryPicture.layer.borderColor = UIColor.lightGray.cgColor
        commentaryPicture.layer.cornerRadius = min(pictureProfile.frame.height, pictureProfile.frame.width)/2.0
        commentaryPicture.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
