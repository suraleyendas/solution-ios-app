//
//  ChallengerViewController.swift
//  LegendService
//
//  Created by informatica on 9/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ChallengerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var dataArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        
        dataArray = ["1","2","3","4","5","6","7","8","9"]

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch dataArray[indexPath.row] {
        case "1":
            let cell = tableView.dequeueReusableCell(withIdentifier: "videoTwoCommChallengerCell", for: indexPath) as! PrototypeTwoChallengerTableViewCell
            
            cell.titleLabel.text = "LEYENDAS EN VIDEO"
            cell.pointsLabel.text = "10 Ptos"
            let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"http://images.apple.com/media/co/macbook-pro/2016/b4a9efaa_6fe5_4075_a9d0_8e4592d6146c/films/design/macbook-pro-design-tft-co-20161026_960x400.mp4\" ></iframe>"
            cell.videoContent.loadHTMLString(htmlStr, baseURL: nil)
            cell.videoContent.scrollView.isScrollEnabled = false
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Este reto ha expirado, puedes participar pero no obtendras puntos"
            
            return cell
        case "2":
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageTwoCommChallengerCell", for: indexPath) as! PrototypeOneChallengerTableViewCell
            
            cell.titleLabel.text = "LEYENDAS EN IMAGEN"
            cell.pointsLabel.text = "80 Ptos"
            cell.imageContent.image = UIImage(named: "camera_men.jpg")
            cell.imageContent.clipsToBounds = true
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Fecha límite 22/04/2017"
            
            return cell
        case "3":
            let cell = tableView.dequeueReusableCell(withIdentifier: "textTwoCommChallengerCell", for: indexPath) as! PrototypeThreeChallengerTableViewCell
            
            cell.titleLabel.text = "LEYENDAS EN TEXTO"
            cell.pointsLabel.text = "50 Ptos"
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
            
        case "4":
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageOneCommChallengerCell", for: indexPath) as! PrototypeFourChallengerTableViewCell
            
            cell.titleLabel.text = "LEYENDAS EN IMAGEN"
            cell.pointsLabel.text = "80 Ptos"
            cell.imageContent.image = UIImage(named: "camera_men.jpg")
            cell.imageContent.clipsToBounds = true
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
        case "5":
            let cell = tableView.dequeueReusableCell(withIdentifier: "videoOneCommChallengerCell", for: indexPath) as! PrototypeFiveChallengerTableViewCell
            
            let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"http://images.apple.com/media/co/macbook-pro/2016/b4a9efaa_6fe5_4075_a9d0_8e4592d6146c/films/design/macbook-pro-design-tft-co-20161026_960x400.mp4\" ></iframe>"
            cell.videoContent.loadHTMLString(htmlStr, baseURL: nil)
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
        case "6":
            let cell = tableView.dequeueReusableCell(withIdentifier: "textOneCommChallengerCell", for: indexPath) as! PrototypeSixChallengerTableViewCell
            
            cell.titleLabel.text = "LEYENDAS EN TEXTO"
            cell.pointsLabel.text = "50 Ptos"
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
        case "7":
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageChallengerCell", for: indexPath) as! PrototypeSevenChallengerTableViewCell
            
            cell.titleLabel.text = "LEYENDAS EN IMAGEN"
            cell.pointsLabel.text = "80 Ptos"
            cell.imageContent.image = UIImage(named: "camera_men.jpg")
            cell.imageContent.clipsToBounds = true
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
        case "8":
            let cell = tableView.dequeueReusableCell(withIdentifier: "videoChallengerCell", for: indexPath) as! PrototypeEightChallengerTableViewCell
            
            let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"http://images.apple.com/media/co/macbook-pro/2016/b4a9efaa_6fe5_4075_a9d0_8e4592d6146c/films/design/macbook-pro-design-tft-co-20161026_960x400.mp4\" ></iframe>"
            cell.videoContent.loadHTMLString(htmlStr, baseURL: nil)
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
        case "9":
            let cell = tableView.dequeueReusableCell(withIdentifier: "textChallengerCell", for: indexPath) as! PrototypeNineChallengerTableViewCell
            
            cell.titleLabel.text = "LEYENDAS EN TEXTO"
            cell.pointsLabel.text = "50 Ptos"
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageTwoCommChallengerCell", for: indexPath) as! PrototypeOneChallengerTableViewCell
            
            cell.userComment.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud "
            cell.deadLineLabel.text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis"
            
            return cell
        }
    }
    @IBAction func backFromDetail(_ sender: UIStoryboardSegue) {
        print("doOptions")
    }
}
