//
//  CommentPublicationViewController.swift
//  LegendService
//
//  Created by informatica on 1/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class CommentPublicationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newCommentaryText: UITextView!
    @IBOutlet weak var saveCommentaryBtn: UIButton!
    @IBOutlet weak var newCommentaryContent: UIView!
    
    var dataArray = [Commentary]()
    var publicationId = 0
    var publicationData = Publication()
    var userName = ""
    var pictureProfile = ""
    var keyboardHeight = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarView?.backgroundColor = .white
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        keyboardHeight = 0
        
        //Up view
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        saveCommentaryBtn.isEnabled = false
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        newCommentaryText.layer.borderColor = myColor.cgColor
        newCommentaryText.layer.borderWidth = 1.0
        newCommentaryText.layer.cornerRadius = 5
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadCommentaries()
    }
    
    func loadCommentaries(){
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId": userId,
            "ContentId": publicationId,
            "Top":100
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/GetPublishByContentId", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if let arrayData = dataResp["arrayContent"] as? [[String: Any]] {
                                
                                for publicationObj in arrayData{
                                    self.dataArray = [Commentary]()
                                    
                                    //Add first rows to add publicarion info depend of its type
                                    self.dataArray.append(Commentary())//Publication content
                                    self.dataArray.append(Commentary())//Publication options bar
                                    self.dataArray.append(Commentary())//Publication user commentary
                                    
                                    let publicationCommentaries = publicationObj["ListComment"] as? [[String: Any]]
                                    for publicationCommentary in publicationCommentaries!{
                                        self.dataArray.append(
                                            Commentary.init(
                                                commentParentid: (publicationCommentary["ContentParentId"] as? Int)!,
                                                userId: (publicationCommentary["UserId"] as? Int)!,
                                                userImage: (publicationCommentary["Picture"] as? String)!,
                                                userName: (publicationCommentary["FullName"] as? String)!,
                                                text: (publicationCommentary["StrComment"] as? String)!
                                            )
                                        )
                                    }
                                    
                                    self.publicationData = Publication.init(
                                        id: (publicationObj["ContentId"] as? Int)!,
                                        userId: (publicationObj["UserId"] as? Int)!,
                                        filePath: (publicationObj["FilePath"] as? String)!,
                                        userCommentary: (publicationObj["strContent"] as? String)!,
                                        isActive: (publicationObj["IsActive"] as? Bool)!,
                                        contentTypeId: (publicationObj["ContentTypeId"] as? Int)!,
                                        contentType: (publicationObj["ContentType"] as? String)!,
                                        UserContentTypeId: (publicationObj["UserContentTypeId"] as? Int)!,
                                        UserContentType: (publicationObj["UserContentType"] as? String)!,
                                        isLike: (publicationObj["IsLike"] as? Bool)!,
                                        countLike: (publicationObj["CountLike"] as? Int)!,
                                        countComment: (publicationObj["CountComment"] as? Int)!,
                                        picture: (publicationObj["Picture"] as? String)!,
                                        name: (publicationObj["Name"] as? String)!,
                                        commetaries: self.dataArray
                                    )
                                }
                                self.tableView.reloadData()
                            }
                        }else{
                            Utils().createAlert("Presentamos inconvenientes con el servicio de comentar. Por favor intetelo mas tarde.", self)
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de comentar. Por favor intetelo mas tarde.", self)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let commentary = self.dataArray[indexPath.row]
        
        switch indexPath.row {
            case 0:
                switch publicationData.contentTypeId {
                    case 1:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "commentText", for: indexPath) as! PrototypeCommetTextTableViewCell
                        cell.name.text = userName
                        if(pictureProfile != ""){
                            cell.picture.imageFromServerURL(urlString: pictureProfile)
                        }else{
                            cell.picture.image = UIImage(named:"small_pic_prof.png")!
                        }
                        cell.textStr.text = publicationData.userCommentary
                        return cell
                    case 2:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "commentVideo", for: indexPath) as! PrototypeCommetVideoTableViewCell
                        cell.name.text = userName
                        if(pictureProfile != ""){
                            cell.picture.imageFromServerURL(urlString: pictureProfile)
                        }else{
                            cell.picture.image = UIImage(named:"small_pic_prof.png")!
                        }
                        let htmlStr = "<style>body { margin: 0; padding: 0; width:100%; height:100%}</style><iframe width=\"100%\"; height=\"100%\" scrolling=\"no\" frameBorder=\"0\" \" src=\"\(publicationData.filePath)\" ></iframe>"
                        cell.videoCont.loadHTMLString(htmlStr, baseURL: nil)
                        return cell
                    case 3:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "commentImage", for: indexPath) as! PrototypeCommetImageTableViewCell
                        cell.name.text = userName
                        if(pictureProfile != ""){
                            cell.picture.imageFromServerURL(urlString: pictureProfile)
                        }else{
                            cell.picture.image = UIImage(named:"small_pic_prof.png")!
                        }
                        cell.imageCont.clipsToBounds = true
                        cell.imageCont.imageFromServerURL(urlString: publicationData.filePath)
                        return cell
                    default:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "commentText", for: indexPath) as! PrototypeCommetTextTableViewCell
                        cell.name.text = userName
                        if(pictureProfile != ""){
                            cell.picture.imageFromServerURL(urlString: pictureProfile)
                        }else{
                            cell.picture.image = UIImage(named:"small_pic_prof.png")!
                        }
                        cell.textStr.text = publicationData.userCommentary
                        return cell
                }
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "optionsBarCell", for: indexPath) as!  PrototypeOptionsBarTableViewCell
                if(publicationData.contentTypeId == 4){
                    
                }else{
                    cell.isHidden = true
                }
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userCommentaryCell", for: indexPath) as! PrototypeUserCommentaryTableViewCell
                if(publicationData.contentTypeId != 1 && publicationData.userCommentary != ""){
                    cell.userText.text = publicationData.userCommentary
                }else{
                    cell.isHidden = true
                }
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "commentaryCell", for: indexPath) as! PrototypeCommentaryTableViewCell
                if(commentary.userImage != ""){
                    cell.commentaryImage.image = Utils().decodeImageToBase64(commentary.userImage)
                }else{
                    cell.accessibilityHint = "\(indexPath.row)"
                    cell.commentaryImage.image = UIImage(named:"small_pic_prof.png")!
                }
                cell.commentaryUser.text = commentary.userName
                cell.commentaryText.text = commentary.text
                return cell
        }
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        if(textView.text! == "" || Utils().contentInitialSpaces(textView.text!)){
            saveCommentaryBtn.isEnabled = false
        }
        else{
            saveCommentaryBtn.isEnabled = true
        }
    }
    
    @IBAction func goSaveCommentary(_ sender: Any) {
        saveNewCommentary(sender as! UIButton)
    }
    
    func saveNewCommentary(_ sender: UIButton){
        
        //Hide keyboard
        if(keyboardHeight != 0){
            view.frame.origin.y += keyboardHeight
            view.endEditing(true)
            keyboardHeight = 0
        }
        
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId": userId,
            "File":"",
            "Content": self.newCommentaryText.text,
            "IsActive":true,
            "ContentParent": publicationId,
            "ContentTypeId": "5"
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/SaveContent", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if(dataResp["Result"] as! Bool){
                                self.newCommentaryText.text = ""
                                self.loadCommentaries()
                            }else{
                                Utils().createAlert("Presentamos inconvenientes con el servicio de comentar. Por favor intetelo mas tarde.", self)
                            }
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de comentar. Por favor intetelo mas tarde.", self)
                }
            }
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                keyboardHeight = keyboardSize.height
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    @IBAction func backCommentPublication(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

