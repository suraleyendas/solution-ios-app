//
//  PrototypeNineChallengerTableViewCell.swift
//  LegendService
//
//  Created by informatica on 10/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class PrototypeNineChallengerTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var imageContent: UIImageView!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var userComment: UILabel!
    @IBOutlet weak var commentCounter: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var imageConst: NSLayoutConstraint!
    @IBOutlet weak var deadLineLabel: UILabel!
    @IBOutlet weak var showDetailsBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

