//
//  EventViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 3/1/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class EventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    var dataArray = [Event]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId":userId
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/GetEventsByUser", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let isEmptyResult = returnData["empty"] as? Bool {
                            if(!isEmptyResult){
                                if let dataResp = returnData["data"] as? [String: Any] {
                                    if(dataResp["Result"] as! Bool){
                                        if let arrayObj = dataResp["arrayEvents"] as? [[String: Any]] {
                                            self.dataArray = [Event]()
                                            for eventObj in arrayObj {
                                                self.dataArray.append(
                                                    Event.init(
                                                        userId: eventObj["UserId"] as! Int,
                                                        userName: eventObj["Name"] as! String,
                                                        userPicture: eventObj["Picture"] as! String,
                                                        id: eventObj["ContentId"] as! Int,
                                                        publishDate: eventObj["PublishDate"] as! String,
                                                        filePath: eventObj["FilePath"] as! String,
                                                        text: eventObj["strContent"] as! String,
                                                        isVieved: eventObj["IsViewed"] as! Bool
                                                    )
                                                )
                                            }
                                            self.tableView.reloadData()
                                            self.stopAnimating()
                                        }
                                    }
                                }
                            }else{
                                self.stopAnimating()
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let event = dataArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventTableViewCell
        if(event.userPicture != ""){
            cell.userPictureProfile.image = Utils().decodeImageToBase64(event.userPicture)
        }else{
            cell.userPictureProfile.image = UIImage(named:"small_pic_prof.png")!
        }
        cell.userName.text = event.userName
        cell.eventDescription.text = event.text
        cell.tag = event.id
        cell.accessibilityHint = event.userPicture
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath) as! EventTableViewCell
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CommentPublicationViewController") as! CommentPublicationViewController
        nextViewController.publicationId = cell.tag
        nextViewController.userName = cell.userName.text!
        nextViewController.pictureProfile = cell.accessibilityHint!
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    @IBAction func backEvent(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
