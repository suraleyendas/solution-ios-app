//
//  PrototypeEightTableViewCell.swift
//  LegendService
//
//  Created by informatica on 27/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

//Text + one commentaries

import UIKit

class PrototypeEightTableViewCell: UITableViewCell {

    @IBOutlet weak var pictureProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var textContent: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var commentaryBtn: UIButton!
    @IBOutlet weak var commentaryCounter: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var commentaryPictureProfile: UIImageView!
    @IBOutlet weak var commentaryUserName: UILabel!
    @IBOutlet weak var commentaryText: UILabel!
    @IBOutlet weak var optionsBar: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pictureProfile.layer.borderWidth = 2
        pictureProfile.layer.masksToBounds = false
        pictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfile.layer.cornerRadius = min(pictureProfile.frame.height, pictureProfile.frame.width)/2.0
        pictureProfile.clipsToBounds = true
        
        commentaryPictureProfile.layer.borderWidth = 2
        commentaryPictureProfile.layer.masksToBounds = false
        commentaryPictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        commentaryPictureProfile.layer.cornerRadius = min(commentaryPictureProfile.frame.height, commentaryPictureProfile.frame.width)/2.0
        commentaryPictureProfile.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
