//
//  ConditionsViewController.swift
//  LegendService
//
//  Created by informatica on 14/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ConditionsViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var agreeCondButton: UIButton!
    @IBOutlet weak var goLoginBtn: UIButton!
    @IBOutlet weak var conditionsScrollLabel: UITextView!
    
    var agreeCond = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //From background
        NotificationCenter.default.addObserver(self, selector:#selector(loadData), name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        userDefaults.set(1, forKey: "root_view")
        
        let parameters = [
            "ParameterId": 1,
            "ProgramId": 1
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ParameterAPI/TermsAndConditions", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    if let jsonData = returnData["data"] as? [String: Any] {
                        DispatchQueue.main.sync(execute: {
                            self.conditionsScrollLabel.text =  String(describing: jsonData["Value"])
                            self.stopAnimating()
                        });
                    }
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func agreeConditions(_ sender: Any) {
        if(agreeCond){
            agreeCond = false;
            goLoginBtn.isEnabled = false;
            goLoginBtn.setImage(UIImage(named: "cond_but_off.png") as UIImage?, for: .normal)
            agreeCondButton.setImage(UIImage(named: "cond_check_off.png") as UIImage?, for: .normal)
        }else{
            agreeCond = true;
            goLoginBtn.isEnabled = true;
            goLoginBtn.setImage(UIImage(named: "cond_but_on.png") as UIImage?, for: .normal)
            agreeCondButton.setImage(UIImage(named: "cond_check_on.png") as UIImage?, for: .normal)
        }
    }
    
}
