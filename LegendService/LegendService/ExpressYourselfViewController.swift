//
//  ExpressYourselfViewController.swift
//  LegendService
//
//  Created by informatica on 23/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ExpressYourselfViewController: UIViewController, UITextViewDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var publishBtnExpYour: UIButton!
    @IBOutlet weak var userNameExpYour: UILabel!
    @IBOutlet weak var pictureProfileExpYour: UIImageView!
    @IBOutlet weak var newTextExpYour: UITextView!
    
    var image = UIImage()
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pictureProfileExpYour.image = image
        userNameExpYour.text = name
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        newTextExpYour.layer.borderColor = myColor.cgColor
        newTextExpYour.layer.borderWidth = 1.0
        newTextExpYour.layer.cornerRadius = 5.0
        
        //Set image properties
        pictureProfileExpYour.layer.borderWidth = 2.0
        pictureProfileExpYour.layer.masksToBounds = true
        pictureProfileExpYour.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfileExpYour.clipsToBounds = true
        
        publishBtnExpYour.isEnabled = false
        publishBtnExpYour.setTitleColor(UIColor .gray, for: UIControlState.normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func goPublish(_ sender: Any) {
        publisExpressYourSelf()
    }
    
    func publisExpressYourSelf(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId": userId,
            "File":"",
            "Content":newTextExpYour.text!,
            "IsActive":true,
            "ContentParent":"",
            "ContentTypeId":"1",
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "ContentAPI/SaveContent", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String: Any] {
                            if(dataResp["Result"] as! Bool){
                                self.stopAnimating()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.publisExpressYourSelf);
                }
            }
        }
    }

    @IBAction func backExpYour(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        pictureProfileExpYour.layer.cornerRadius = min(pictureProfileExpYour.bounds.size.height, pictureProfileExpYour.bounds.size.width)/2.0
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        if(textView.text! == "" || Utils().contentInitialSpaces(textView.text!)){
            publishBtnExpYour.isEnabled = false
            publishBtnExpYour.setTitleColor(UIColor .gray, for: UIControlState.normal)
        }else{
            publishBtnExpYour.isEnabled = true
            publishBtnExpYour.setTitleColor(UIColor .black, for: UIControlState.normal)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
