//
//  PrototypeTwoTableViewCell.swift
//  LegendService
//
//  Created by informatica on 27/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

//Image + one commentaries

import UIKit

class PrototypeTwoTableViewCell: UITableViewCell {

    @IBOutlet weak var pictureProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imageContent: UIImageView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var commentaryBtn: UIButton!
    @IBOutlet weak var commentaryCounter: UILabel!
    @IBOutlet weak var userCommentary: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var commentPicture: UIImageView!
    @IBOutlet weak var commentName: UILabel!
    @IBOutlet weak var commentText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pictureProfile.layer.borderWidth = 2
        pictureProfile.layer.masksToBounds = false
        pictureProfile.layer.borderColor = UIColor.lightGray.cgColor
        pictureProfile.layer.cornerRadius = min(pictureProfile.frame.height, pictureProfile.frame.width)/2.0
        pictureProfile.clipsToBounds = true
        
        commentPicture.layer.borderWidth = 2
        commentPicture.layer.masksToBounds = false
        commentPicture.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
