//
//  MyProfileViewController.swift
//  LegendService
//
//  Created by informatica on 15/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class DefaultProfileViewController: UIViewController, NVActivityIndicatorViewable{
    
    @IBOutlet weak var pictureProfileView: UIImageView!
    @IBOutlet weak var followNumberLabel: UILabel!
    @IBOutlet weak var followerNumberLabel: UILabel!
    @IBOutlet weak var votesNumberLabel: UILabel!
    @IBOutlet weak var pointNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var comapnyCityLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var voteForLegendBtn: UIButton!
    @IBOutlet weak var followName: UILabel!
    @IBOutlet weak var FollowerName: UILabel!
    @IBOutlet weak var voteName: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    let userDefaults = UserDefaults.standard
    
    var isMyProfile = false
    var userProfileId = 0
    var userProfileIsFollow = true;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add gesture to labels
        let tapFollow = UITapGestureRecognizer(target: self, action: #selector(goFollowViewController))
        followNumberLabel.isUserInteractionEnabled = true
        followNumberLabel.addGestureRecognizer(tapFollow)
        let tapFollowName = UITapGestureRecognizer(target: self, action: #selector(goFollowViewController))
        followName.isUserInteractionEnabled = true
        followName.addGestureRecognizer(tapFollowName)
        let tapFollower = UITapGestureRecognizer(target: self, action: #selector(goFollowerViewController))
        followerNumberLabel.isUserInteractionEnabled = true
        followerNumberLabel.addGestureRecognizer(tapFollower)
        let tapFollowerName = UITapGestureRecognizer(target: self, action: #selector(goFollowerViewController))
        FollowerName.isUserInteractionEnabled = true
        FollowerName.addGestureRecognizer(tapFollowerName)
        let tapVotes = UITapGestureRecognizer(target: self, action: #selector(goVotesViewController))
        votesNumberLabel.isUserInteractionEnabled = true
        votesNumberLabel.addGestureRecognizer(tapVotes)
        let tapVotesName = UITapGestureRecognizer(target: self, action: #selector(goVotesViewController))
        voteName.isUserInteractionEnabled = true
        voteName.addGestureRecognizer(tapVotesName)
        
        if(userProfileIsFollow){
            followBtn.setImage(UIImage(named: "follow_fill_btn.png") as UIImage?, for: .normal)
        }else{
            followBtn.setImage(UIImage(named: "follow_empty_btn.png") as UIImage?, for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadData()
        loadLegendValues()
        scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func loadData(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetUser/\(userProfileId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            let picPrif = (response["Picture"] as? String)!
                            if(picPrif != ""){
                                self.loadPictureProfile(picPrif);
                            }else{
                                let image : UIImage = UIImage(named:"big_pic_prof.jpg")!
                                self.pictureProfileView.image = image
                            }
                            self.nameLabel.text = (response["Name"] as? String)!
                            self.emailLabel.text = (response["Email"] as? String)!
                            self.comapnyCityLabel.text = "\((response["Company"] as? String)!) - \((response["City"] as? String)!)"
                            self.summaryLabel.text = (response["Summary"] as? String)!
                            self.stopAnimating()
                            self.loadLegendVoteState()
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }
        
    }
    
    func loadLegendVoteState(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId":userId,
            "UserVotedId":userProfileId
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/UserIsVoted", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            if(response["Result"] as? Bool)!{
                                self.voteForLegendBtn.setImage(UIImage(named: "vote_legend_fill_btn.png") as UIImage?, for: .normal)
                            }else{
                                self.voteForLegendBtn.setImage(UIImage(named: "vote_legend_empty_btn.png") as UIImage?, for: .normal)
                            }
                            self.stopAnimating()
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.loadData);
                }
            }
        }

    }
    
    func loadLegendValues(){
        let parameters = [:] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/GetProfileInfoByUserId/\(userProfileId)", "GET") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let response = returnData["data"] as? [String: Any] {
                            self.pointNumberLabel.text = "\((response["Points"] as? Int)!)"
                            self.followNumberLabel.text = "\((response["Follows"] as? Int)!)"
                            self.followerNumberLabel.text = "\((response["Followers"] as? Int)!)"
                            self.votesNumberLabel.text = "\((response["Votes"] as? Int)!)"
                        }else{
                            self.pointNumberLabel.text = "0"
                            self.followNumberLabel.text = "0"
                            self.followerNumberLabel.text = "0"
                            self.votesNumberLabel.text = "0"
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de valores de leyenda. Por favor intetelo mas tarde.", self)
                }
            }
        }

    }
    
    func loadPictureProfile(_ imageUrlString : String){
        let imageUrl:URL = URL(string: imageUrlString)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        self.pictureProfileView.image = image
    }
    
    func goFollowViewController(){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FollowViewController") as! FollowViewController
        nextViewController.userId = userProfileId
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func goFollowerViewController(){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FollowersViewController") as! FollowersViewController
        nextViewController.userId = userProfileId
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func goVotesViewController(){
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "VotesViewController") as! VotesViewController
        nextViewController.userId = userProfileId
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goConfirmVote(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ConfirmVoteViewController") as! ConfirmVoteViewController
        nextViewController.userToVoteId = userProfileId
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func followAction(_ sender: Any) {
        
        let userId = self.userDefaults.integer(forKey: "user_id")
        
        let parameters = [
            "UserId":userId ,
            "UserFollowId": userProfileId,
            "IsFollow": !userProfileIsFollow
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/SaveUserFollow", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if(!self.userProfileIsFollow){
                            self.followBtn.setImage(UIImage(named: "follow_fill_btn.png") as UIImage?, for: .normal)
                            self.userProfileIsFollow = true
                        }else{
                            self.followBtn.setImage(UIImage(named: "follow_empty_btn.png") as UIImage?, for: .normal)
                            self.userProfileIsFollow = false
                        }
                    });
                }else{
                    Utils().createAlert("Presentamos inconvenientes con el servicio de seguir compañeros. Por favor intetelo mas tarde.", self)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
