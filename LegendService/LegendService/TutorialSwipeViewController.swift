//
//  TutorialSwipeViewController.swift
//  LegendService
//
//  Created by Julio Cesar Diaz M on 3/4/17.
//  Copyright © 2017 informatica. All rights reserved.
//  Show tutuorial master

import UIKit

class TutorialSwipeViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainDescription: UILabel!
    @IBOutlet weak var swipeView: UIView!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var forwardBtn: UIButton!
    @IBOutlet weak var pageCount: UIPageControl!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var titleConstr: NSLayoutConstraint!
    @IBOutlet weak var imageUpConst: NSLayoutConstraint!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    @IBOutlet weak var topImage: NSLayoutConstraint!
    
    var titleUpper = ""
    var descriptions = [NSMutableAttributedString]()
    var images = [String]()
    var count = 0
    var isFinish = false
    var labelTextOne = UILabel()
    var constUpImage = NSLayoutConstraint()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        constUpImage = imageUpConst
        startBtn.isHidden = true
        mainImage.clipsToBounds = true
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 17)]
        images = ["t_one_img.png", "t_two_img.png", "t_three_img.png", "t_four_img.png", "t_five_img.png"];
        
        titleUpper = "¡Es tiempo de crear \n tu propia Leyenda!"
        
        let textOne = NSMutableAttributedString(string: "para que tengas a la mano historias, tips de servicio y todo el contenido que quieres ver en tiempo real.")
        let textOneBold = NSMutableAttributedString(string:"Leyendas evoluciona para estar a tu lado, ", attributes:attrs)
        textOneBold.append(textOne)
        descriptions.append(textOneBold)
        
        let textTwo = NSMutableAttributedString(string: "Ingresa a la app, crea tu perfil y conéctate con otras Leyendas e ")
        let textTwoBold = NSMutableAttributedString(string:"interactuar con ellas en tiempo real", attributes:attrs)
        textTwo.append(textTwoBold)
        descriptions.append(textTwo)
        
        let textThree = NSMutableAttributedString(string: "comparte información, fotos y videos. " )
        let textThreeBold = NSMutableAttributedString(string: "Crea contenidos, ", attributes:attrs)
        textThreeBold.append(textThree)
        descriptions.append(textThreeBold)
        
        let textFour = NSMutableAttributedString(string: "Participa en los retos y suma puntos que podrás redimir en el catálogo de premios.")
        let textFourBold = NSMutableAttributedString(string:" Entre más participes, más puntos sumarás y grandes premios y beneficios obtendrás. ", attributes:attrs)
        textFourBold.append(textFour)
        descriptions.append(textFourBold)
        
        let textFiveUp = NSMutableAttributedString(string: "¡Vota por la persona que crees merece ser la Leyenda Ejemplar de la compañia ")
        let textFiveBoldUp = NSMutableAttributedString(string:"y dinos porqué se lo merece, esto también te da puntos! \n\n", attributes:attrs)
        let textFiveBoldDown = NSMutableAttributedString(string:"¡Aquí encontrarás todo lo que necesitas para \n ser una Leyenda del Servicio!", attributes:attrs)
        textFiveUp.append(textFiveBoldUp)
        textFiveUp.append(textFiveBoldDown)
        descriptions.append(textFiveUp)
        
        mainDescription.attributedText = descriptions[0]
        mainImage.image = UIImage(named: images[0])
        
        let gestureRight = UISwipeGestureRecognizer(target: self, action: #selector(right))
        gestureRight.direction = .right
        gestureRight.delegate = self
        view.addGestureRecognizer(gestureRight)
        
        let gestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(left))
        gestureLeft.direction = .left
        gestureLeft.delegate = self
        view.addGestureRecognizer(gestureLeft)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func right(gesture: UIGestureRecognizer){
        if(count > 0){
            if(count != 3){
                startBtn.isHidden = true
                skipBtn.isHidden = false
                forwardBtn.isHidden = false
                imageUpConst = constUpImage
                imageUpConst.isActive = true
                topImage.constant = 0
            }
            
            mainDescription.rightToLeftAnimation()
            mainDescription.attributedText = descriptions[count - 1]
            mainImage.rightToLeftAnimation()
            mainImage.image = UIImage(named: images[count - 1])
            count = count - 1
            pageCount.currentPage = count
            
            if(count > 0){
                mainTitle.isHidden = true
            }else{
                mainTitle.isHidden = false
            }
        }
    }
    
    func left(gesture: UIGestureRecognizer){
        if(count < (descriptions.count - 1)){
            if(count == 3){
                startBtn.isHidden = false
                skipBtn.isHidden = true
                forwardBtn.isHidden = true
                imageUpConst.isActive = false
                topImage.constant = UIScreen.main.bounds.height * 0.03
            }
            
            mainDescription.leftToRightAnimation()
            mainDescription.attributedText = descriptions[count + 1]
            mainImage.leftToRightAnimation()
            mainImage.image = UIImage(named: images[count + 1])
            count = count + 1
            pageCount.currentPage = count
            
            if(count > 0){
                mainTitle.isHidden = true
            }else{
                mainTitle.isHidden = false
            }
        }
    }
    
    @IBAction func nextPage(_ sender: Any) {
        left(gesture: UIGestureRecognizer())
    }
}

extension UIView {
    func leftToRightAnimation(_ duration: TimeInterval = 0.5, completionDelegate: AnyObject? = nil) {
        // Create a CATransition object
        let leftToRightTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided
        if let del = completionDelegate as! CAAnimationDelegate? {
            leftToRightTransition.delegate = del
        }
        
        leftToRightTransition.type = kCATransitionPush
        leftToRightTransition.subtype = kCATransitionFromRight
        leftToRightTransition.duration = duration
        leftToRightTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        leftToRightTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(leftToRightTransition, forKey: "leftToRightTransition")
    }
    
    func rightToLeftAnimation(_ duration: TimeInterval = 0.5, completionDelegate: AnyObject? = nil) {
        // Create a CATransition object
        let rightToLeftAnimation = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided
        if let del = completionDelegate as! CAAnimationDelegate? {
            rightToLeftAnimation.delegate = del
        }
        
        rightToLeftAnimation.type = kCATransitionPush
        rightToLeftAnimation.subtype = kCATransitionFromLeft
        rightToLeftAnimation.duration = duration
        rightToLeftAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        rightToLeftAnimation.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(rightToLeftAnimation, forKey: "rightToLeftAnimation")
    }
}
