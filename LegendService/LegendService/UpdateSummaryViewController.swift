//
//  UpdateSummaryViewController.swift
//  LegendService
//
//  Created by informatica on 21/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class UpdateSummaryViewController: UIViewController, UITextViewDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var profileImageSummary: UIImageView!
    @IBOutlet weak var nameSummary: UILabel!
    @IBOutlet weak var newSummary: UITextView!
    @IBOutlet weak var publicateBtn: UIButton!
    
    var image = UIImage()
    var name = ""
    var currentSummary = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImageSummary.image = image
        nameSummary.text = name
        newSummary.text = currentSummary
        
        //Add border text
        let myColor : UIColor = UIColor.gray
        newSummary.layer.borderColor = myColor.cgColor
        newSummary.layer.borderWidth = 1.0
        newSummary.layer.cornerRadius = 5.0
        
        //Set image properties
        profileImageSummary.layer.borderWidth = 2.0
        profileImageSummary.layer.masksToBounds = true
        profileImageSummary.layer.borderColor = UIColor.lightGray.cgColor
        profileImageSummary.clipsToBounds = true
        
        publicateBtn.isEnabled = false
        publicateBtn.setTitleColor(UIColor .gray, for: UIControlState.normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        profileImageSummary.layer.cornerRadius = min(profileImageSummary.bounds.size.height, profileImageSummary.bounds.size.width)/2.0
    }
    
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        if(textView.text == ""){
            publicateBtn.isEnabled = true
            publicateBtn.setTitleColor(UIColor .black, for: UIControlState.normal)
        }else if(Utils().contentInitialSpaces(textView.text!)){
            publicateBtn.isEnabled = false
            publicateBtn.setTitleColor(UIColor .gray, for: UIControlState.normal)
        }
        else{
            publicateBtn.isEnabled = true
            publicateBtn.setTitleColor(UIColor .black, for: UIControlState.normal)
        }
    }
    
    @IBAction func goPublish(_ sender: Any) {
        publishSummary()
    }
    
    func publishSummary(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Espere por favor", type: NVActivityIndicatorType(rawValue:29)!)
        let userDefaults = UserDefaults.standard
        let userId = userDefaults.integer(forKey: "user_id")
        let parameters = [
            "UserId": userId,
            "Summary": newSummary.text!
            ] as [String : Any]
        
        ServerHandler().sendRequest(parameters, "UsersAPI/UpdateSummaryByUserId", "POST") {
            returnData in
            print("Data \(returnData)")
            if let status = returnData["status"] as? Bool {
                if(status){
                    DispatchQueue.main.sync(execute: {
                        if let dataResp = returnData["data"] as? [String:Any]{
                            if(dataResp["Result"] as! Bool){
                                self.stopAnimating()
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    });
                }else{
                    DispatchQueue.main.sync(execute: {
                        self.stopAnimating()
                    })
                    Utils().createDissmisAlertWithRetry("En este momento presentamos inconvenientes con el servicio.", self, self.publishSummary);
                }
            }
        }
    }

    @IBAction func backUpdateSummary(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
