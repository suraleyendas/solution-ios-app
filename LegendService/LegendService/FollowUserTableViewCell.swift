//
//  FollowUserTableViewCell.swift
//  LegendService
//
//  Created by informatica on 20/02/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class FollowUserTableViewCell: UITableViewCell {

    @IBOutlet weak var followImage: UIImageView!
    @IBOutlet weak var followName: UILabel!
    @IBOutlet weak var followEmail: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        followImage.layer.borderWidth = 2
        followImage.layer.masksToBounds = false
        followImage.layer.borderColor = UIColor.lightGray.cgColor
        followImage.layer.cornerRadius = min(followImage.frame.height, followImage.frame.width)/2.0
        followImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
