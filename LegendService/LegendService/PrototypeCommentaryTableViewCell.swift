//
//  PrototypeCommentaryTableViewCell.swift
//  LegendService
//
//  Created by informatica on 1/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class PrototypeCommentaryTableViewCell: UITableViewCell {

    @IBOutlet weak var commentaryImage: UIImageView!
    @IBOutlet weak var commentaryUser: UILabel!
    @IBOutlet weak var commentaryText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commentaryImage.layer.borderWidth = 2
        commentaryImage.layer.masksToBounds = false
        commentaryImage.layer.borderColor = UIColor.lightGray.cgColor
        commentaryImage.layer.cornerRadius = min(commentaryImage.frame.height, commentaryImage.frame.width)/2.0
        commentaryImage.clipsToBounds = true}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
