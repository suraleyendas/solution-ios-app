//
//  PrototypeTwoChallengerTableViewCell.swift
//  LegendService
//
//  Created by informatica on 9/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//
//  Video with two commentaries

import UIKit

class PrototypeTwoChallengerTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var likeCounter: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var userComment: UILabel!
    @IBOutlet weak var commentCounter: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var topConst: NSLayoutConstraint!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var videoContent: UIWebView!
    @IBOutlet weak var deadLineLabel: UILabel!
    @IBOutlet weak var showDetailsBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var commentOnePicture: UIImageView!
    @IBOutlet weak var commenteOneUser: UILabel!
    @IBOutlet weak var commentOneText: UILabel!
    @IBOutlet weak var commentTwoPicture: UIImageView!
    @IBOutlet weak var commentTwoUser: UILabel!
    @IBOutlet weak var commentTwoText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commentOnePicture.layer.borderWidth = 2
        commentOnePicture.layer.masksToBounds = false
        commentOnePicture.layer.borderColor = UIColor.lightGray.cgColor
        commentOnePicture.layer.cornerRadius = min(commentOnePicture.bounds.width/2, commentOnePicture.layer.bounds.height/2)
        commentOnePicture.clipsToBounds = true
        
        commentTwoPicture.layer.borderWidth = 2
        commentTwoPicture.layer.masksToBounds = false
        commentTwoPicture.layer.borderColor = UIColor.lightGray.cgColor
        commentTwoPicture.layer.cornerRadius = min(commentOnePicture.bounds.width/2, commentOnePicture.layer.bounds.height/2)
        commentTwoPicture.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
