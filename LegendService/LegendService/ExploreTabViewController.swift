//
//  ExploreTabViewController.swift
//  LegendService
//
//  Created by informatica on 9/03/17.
//  Copyright © 2017 informatica. All rights reserved.
//

import UIKit

class ExploreTabViewController: UITabBarController {
    
    var itemChoosed = 0
    
    override func viewDidLoad() {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(6, forKey: "root_view")
        
        super.viewDidLoad()
        self.selectedViewController = self.viewControllers?[itemChoosed]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBar.tintColor = UIColor.white
        tabBar.backgroundColor = UIColor.white
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
        let width = tabBar.bounds.width
        var selectionImage = UIImage(named:"item_selected_background.png")
        let tabSize = CGSize(width: width/4, height: tabBar.bounds.height)
        
        UIGraphicsBeginImageContext(tabSize)
        selectionImage?.draw(in: CGRect(x: 0, y: 0, width: tabSize.width, height: tabSize.height))
        selectionImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        tabBar.selectionIndicatorImage = selectionImage
    }

}
